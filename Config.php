<?php

namespace xtetis\xdate;

class Config extends \xtetis\xengine\models\Model
{
    /**
     * Проверяет параметры
     */
    public static function validateParams()
    {
        if (!defined('ID_DATE_ALBUM_PARENT_CATEGORY'))
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Не определена константа ID_DATE_ALBUM_PARENT_CATEGORY');
        }

        if (!is_integer(ID_DATE_ALBUM_PARENT_CATEGORY))
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Константа ID_PARTY_GALLERY_PARENT_CATEGORY должна быть сущесвующей категорией');
        }

        if (!ID_DATE_ALBUM_PARENT_CATEGORY)
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Константа ID_PARTY_GALLERY_PARENT_CATEGORY должна быть сущесвующей категорией');
        }

        $model_gallery_category = \xtetis\ximg\models\CategoryModel::generateModelById(ID_DATE_ALBUM_PARENT_CATEGORY);
        if (!$model_gallery_category)
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Константа ID_DATE_ALBUM_PARENT_CATEGORY должна быть сущесвующей категорией');
        }

        if (!defined('ID_PARTY_GALLERY_PARENT_CATEGORY'))
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Не определена константа ID_PARTY_GALLERY_PARENT_CATEGORY');
        }

        if (!is_integer(ID_PARTY_GALLERY_PARENT_CATEGORY))
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Константа ID_PARTY_GALLERY_PARENT_CATEGORY должна быть сущесвующей категорией');
        }

        if (!ID_PARTY_GALLERY_PARENT_CATEGORY)
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Константа ID_PARTY_GALLERY_PARENT_CATEGORY должна быть сущесвующей категорией');
        }

        $model_gallery_category = \xtetis\ximg\models\CategoryModel::generateModelById(ID_PARTY_GALLERY_PARENT_CATEGORY);
        if (!$model_gallery_category)
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Константа ID_PARTY_GALLERY_PARENT_CATEGORY должна быть сущесвующей категорией');
        }
    }

}
