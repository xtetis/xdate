<?php

/**
 * Рендер списка профилей (знакомства)
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'list');

// Получаем параметры
//-----------------------------------------------------
// Тип профиля знакомств
$id_profile_type = \xtetis\xengine\helpers\RequestHelper::get('id_profile_type', 'int', 0);
// Текущая страница
$page = \xtetis\xengine\helpers\RequestHelper::get('p', 'int', 1);
// Возраст от
$age_start = \xtetis\xengine\helpers\RequestHelper::get('age_start', 'int', 0);
// Возраст до
$age_end = \xtetis\xengine\helpers\RequestHelper::get('age_end', 'int', 0);
// Локация
$id_location       = \xtetis\xengine\helpers\RequestHelper::get('id_location', 'int', 0);
$id_location_level = \xtetis\xengine\helpers\RequestHelper::get('id_location_level', 'int', 0);
//-----------------------------------------------------

// Текущая страница
//-----------------------------------------------------
if ($page < 1)
{
    $page = 1;
}
//-----------------------------------------------------

// Получаем список моделей профилей знакомств
//-----------------------------------------------------
$model_data_profile_list = new \xtetis\xdate\models\ProfileListModel([
    'offset'            => (20 * ($page - 1)),
    'id_profile_type'   => $id_profile_type,
    'age_start'         => $age_start,
    'age_end'           => $age_end,
    'id_location'       => $id_location,
    'id_location_level' => $id_location_level,
]);
$model_data_profile_list->getProfileModelList();
if ($model_data_profile_list->getErrors())
{
    http_response_code(404);
    \xtetis\xengine\helpers\LogHelper::customDie($model_data_profile_list->getLastErrorMessage());
}
//-----------------------------------------------------

// Урлы
//-----------------------------------------------------
$urls['url_date'] = self::makeUrl();

// Урл поиска для пагинации
$query = [];
if ($model_data_profile_list->id_profile_type)
{
    $query['id_profile_type'] = $model_data_profile_list->id_profile_type;
}
if ($model_data_profile_list->age_start)
{
    $query['age_start'] = $model_data_profile_list->age_start;
}
if ($model_data_profile_list->age_end)
{
    $query['age_end'] = $model_data_profile_list->age_end;
}
if ($model_data_profile_list->id_location)
{
    $query['id_location'] = $model_data_profile_list->id_location;
}

$urls['url_search'] = \xtetis\xdate\Component::makeUrl([
    'path'  => [
        \xtetis\xengine\App::getApp()->getAction(),
    ],
    'query' => $query,
]);
//-----------------------------------------------------


// Пагинация
//-----------------------------------------------------
$paginate = \xtetis\xengine\helpers\PaginateHelper::getPagination(
    $model_data_profile_list->limit,
    $page,
    $model_data_profile_list->total_count,
    $urls['url_search']
);
//-----------------------------------------------------

// Устанавливаем Title страницы
\xtetis\xengine\helpers\SeoHelper::setTitle('Знакомства');

// Устанавливаем Name страницы
\xtetis\xengine\helpers\SeoHelper::setPageName('Знакомства');

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage([
    'urls'                    => $urls,
    'model_data_profile_list' => $model_data_profile_list,
    'paginate'                => $paginate,
]);
