<?php

/**
 * Добавляет альбом профиля знакомств
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$response = [
    'result' => false,
];

// Получаем параметры
//*****************************************************
$id_profile_type   = \xtetis\xengine\helpers\RequestHelper::post('id_profile_type', 'int', 0);
$age_start         = \xtetis\xengine\helpers\RequestHelper::post('age_start', 'int', 0);
$age_end           = \xtetis\xengine\helpers\RequestHelper::post('age_end', 'int', 0);
$id_location       = \xtetis\xengine\helpers\RequestHelper::post('id_location', 'int', 0);
//*****************************************************

$model_data_profile_list = new \xtetis\xdate\models\ProfileListModel(
    [
        'id_profile_type'   => $id_profile_type,
        'age_start'         => $age_start,
        'age_end'           => $age_end,
        'id_location'       => $id_location,
    ]
);
$model_data_profile_list->validateParams();
if ($model_data_profile_list->getErrors())
{
    $response['errors'] = $model_data_profile_list->getErrors();
}
else
{
    $query = [];

    if ($id_profile_type)
    {
        $query['id_profile_type'] = $id_profile_type;
    }
    if ($age_start)
    {
        $query['age_start'] = $age_start;
    }
    if ($age_end)
    {
        $query['age_end'] = $age_end;
    }
    if ($id_location)
    {
        $query['id_location'] = $id_location;
    }

    $url_search = \xtetis\xdate\Component::makeUrl([
        'path'  => [],
        'query' => $query,
    ]);

    $response['result']            = true;
    $response['data']['go_to_url'] = $url_search;
}

$response['js_success'] = 'xform.goToUrl();';
$response['js_fail']    = '';

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
