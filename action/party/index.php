<?php

/**
 * Рендер списка вечеринок
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xengine\App::getApp()->setParam('layout', 'list');

// Получаем параметры
//-----------------------------------------------------
// Текущая страница
$page = \xtetis\xengine\helpers\RequestHelper::get('p', 'int', 1);

// Локация
$id_location       = \xtetis\xengine\helpers\RequestHelper::get('id_location', 'int', 0);
$id_location_level = \xtetis\xengine\helpers\RequestHelper::get('id_location_level', 'int', 0);
//-----------------------------------------------------

// Текущая страница
//-----------------------------------------------------
if ($page < 1)
{
    $page = 1;
}
//-----------------------------------------------------

// Поиск вечеринок
//-----------------------------------------------------
$model_date_party_list = new \xtetis\xdate\models\DatePartyListModel([
    'offset' => (18 * ($page - 1)),
]);
$model_date_party_list->getModelList();
//-----------------------------------------------------



// Урлы
//-----------------------------------------------------
$urls['url_date'] = self::makeUrl();
// Добавить вечеринку
$urls['url_party_add'] = self::makeUrl(['path' => ['party','add']]);
    


// Урл поиска для пагинации
$query = [];
/*
if ($model_data_profile_list->id_profile_type)
{
    $query['id_profile_type'] = $model_data_profile_list->id_profile_type;
}
if ($model_data_profile_list->age_start)
{
    $query['age_start'] = $model_data_profile_list->age_start;
}
if ($model_data_profile_list->age_end)
{
    $query['age_end'] = $model_data_profile_list->age_end;
}
if ($model_data_profile_list->id_location)
{
    $query['id_location'] = $model_data_profile_list->id_location;
}
if ($model_data_profile_list->id_location_level)
{
    $query['id_location_level'] = $model_data_profile_list->id_location_level;
}
*/

$urls['url_search'] = \xtetis\xdate\Component::makeUrl([
    'path'  => [
        \xtetis\xengine\App::getApp()->getAction(),
    ],
    'query' => $query,
]);
//-----------------------------------------------------

// Пагинация
//-----------------------------------------------------
$paginate = \xtetis\xengine\helpers\PaginateHelper::getPagination(
    $model_date_party_list->limit,
    $page,
    $model_date_party_list->total_count,
    $urls['url_search']
);
//-----------------------------------------------------

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage([
    'model_date_party_list' => $model_date_party_list,
    'paginate'              => $paginate,
    'urls'                  => $urls,
]);
