<?php

/**
 * Рендер списка профилей
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

// Получаем параметры
//-----------------------------------------------------
$id_party = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);
//-----------------------------------------------------

// Поиск вечеринки
//-----------------------------------------------------
$model_party = \xtetis\xdate\models\DatePartyModel::generateModelById($id_party);
if (!$model_party)
{
    \xtetis\xengine\helpers\LogHelper::customDie('Вечеринка не найдена');
}

// Дата вечеринки
if (strtotime($model_party->party_date) < strtotime('today'))
{
    \xtetis\xengine\helpers\LogHelper::customDie('Вечеринка уже закончилась');
}
//-----------------------------------------------------

//-----------------------------------------------------
$model_gallery_cover = $model_party->getModelRelated('id_gallery_cover');
$model_gallery_place = $model_party->getModelRelated('id_gallery_place');
$model_user_author   = $model_party->getModelRelated('id_user');
//-----------------------------------------------------

// Профиль пользователя
//-----------------------------------------------------
$model_date_profile_author = new \xtetis\xdate\models\ProfileModel([
    'id_user' => $model_user_author->id,
]);
$model_date_profile_author->getByUserId();
if ($model_date_profile_author->getErrors())
{
    \xtetis\xengine\helpers\LogHelper::customDie($model_date_profile_author->getLastErrorMessage());
}
//-----------------------------------------------------

// Урлы
//-----------------------------------------------------
$urls['url_date']           = self::makeUrl();
$urls['url_date_party']     = self::makeUrl(['path' => ['party']]);
//-----------------------------------------------------

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage([
    'model_party'               => $model_party,
    'model_gallery_cover'       => $model_gallery_cover,
    'model_gallery_place'       => $model_gallery_place,
    'model_user_author'         => $model_user_author,
    'model_date_profile_author' => $model_date_profile_author,
    'urls'                      => $urls,
]);
