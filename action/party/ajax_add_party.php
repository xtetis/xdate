<?php

/**
 * Рендер списка профилей
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$response = [
    'result' => false,
];

// Получаем параметры
//*****************************************************
$name              = \xtetis\xengine\helpers\RequestHelper::post('name', 'str', '');
$about             = \xtetis\xengine\helpers\RequestHelper::post('about', 'raw', '');
$party_date        = \xtetis\xengine\helpers\RequestHelper::post('party_date', 'str', '');
$id_city           = \xtetis\xengine\helpers\RequestHelper::post('id_city', 'int', '');
$image_cover       = $_POST['image_cover'] ?? [];
$image_party_place = $_POST['image_party_place'] ?? [];

//*****************************************************

$model_date_party = new \xtetis\xdate\models\DatePartyModel([
    'name'              => $name,
    'about'             => $about,
    'party_date'        => $party_date,
    'id_city'           => $id_city,
    'image_cover'       => $image_cover,
    'image_party_place' => $image_party_place,
]);

if (!$model_date_party->addParty())
{
    $response['errors'] = $model_date_party->getErrors();
}
else
{
    // Устанавливаем параметры для отображения страницы с сообщением для пользователя с единым урлом
    \xtetis\xpagemessage\Component::setPageMessageParams([
        'message' => 'Анонс вечеринки добавлен, после проверки страница будет опубликована',
        'name'    => 'Вечеринка добавлена',
        'title'   => 'Вечеринка добавлена',
    ]);

    $response['result']            = true;
    $response['js_success']        = 'xform.goToUrl();';
    $response['data']['go_to_url'] = \xtetis\xpagemessage\Component::makeUrl();
}

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
