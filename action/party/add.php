<?php

/**
 * Добавление альбома в профиль знакомств
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}


if (!\xtetis\xuser\Component::isLoggedIn())
{
    \xtetis\xengine\helpers\LogHelper::customDie('Только для авторизированных пользователей');
}


$model_date_party = new \xtetis\xdate\models\DatePartyModel();


// Урлы
// --------------------------------------------------
$urls['validate_form'] = \xtetis\xdate\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'ajax_add_party',
    ],
]);
$urls['xgeo_multiple_data_url'] = \xtetis\xgeo\Component::makeUrl([
    'path' => [
        'data', 
        'ajax_js_tree'
        ]
]);

$urls['url_date'] = self::makeUrl();

$urls['url_party'] = self::makeUrl(['path' => ['party']]);
// --------------------------------------------------

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage([
    'urls'             => $urls,
    'model_date_party' => $model_date_party,
]);
