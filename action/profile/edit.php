<?php

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

if (!\xtetis\xuser\Component::isLoggedIn())
{
    \xtetis\xengine\helpers\LogHelper::customDie('Только для авторизированных пользователей');
}

$model_user = \xtetis\xuser\Component::isLoggedIn();

// Получаем профиль
//*****************************************************
$model_date_profile = new \xtetis\xdate\models\ProfileModel([
    'id_user' => $model_user->id,
]);
$model_date_profile->getByUserId();
if ($model_date_profile->getErrors())
{
    \xtetis\xengine\helpers\LogHelper::customDie($model_date_profile->getLastErrorMessage());
}
//*****************************************************

// Тип профиля
//*****************************************************
$date_profile_type_model   = new \xtetis\xdate\models\ProfileTypeModel();
$date_profile_type_options = $date_profile_type_model->getOptions();
//*****************************************************


// Урлы
//*****************************************************
$urls['url_my_date_profile']  = self::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'my',
    ],
]);

$urls['url_validate_profile_edit'] = self::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'ajax_validate_profile_edit',
    ],
]);

$urls['xgeo_multiple_data_url'] = \xtetis\xgeo\Component::makeUrl([
    'path' => [
        'data',
        'ajax_js_tree',
    ],
]);
//*****************************************************

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage([
    'model_date_profile'             => $model_date_profile,
    'date_profile_type_options'      => $date_profile_type_options,
    'model_user'                     => $model_user,
    'urls'                           => $urls,
]);
