<?php

/**
 * Альбом профиля знакомств
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

if (!\xtetis\xuser\Component::isLoggedIn())
{
    \xtetis\xengine\helpers\LogHelper::customDie('Только для авторизированных пользователей');
}

$id = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);

$model_date_album = new \xtetis\xdate\models\DateAlbumModel([
    'id' => $id,
]);

// Получаем значение по ID
$model_date_album->getById();

// Проверяет - принадлежит ли альбом текущему пользователю
$model_date_album->checkIsOwnerCurrentUser();

// ПОлучаем галерею по ID
$model_gallery = $model_date_album->getModelGallery();

if ($model_date_album->getErrors())
{
    \xtetis\xengine\helpers\LogHelper::customDie($model_date_album->getLastErrorMessage());
}

// Урл загрузки изображения в альбом пользователя
$url_upload_image_to_gallery = \xtetis\xdate\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'ajax_upload_image_to_date_album',
        $id,
    ],
]);

// Генерируем урлы для просмотра изображений
$url_image_view_list = [];
// Генерируем урлы для удаления изображений
$url_image_delete_list = [];
foreach ($model_gallery->getImgModelList() as $id_img => $model_img)
{
    $url_image_view_list[$id_img] = \xtetis\xdate\Component::makeUrl([
        'path' => [
            \xtetis\xengine\App::getApp()->getAction(),
            'my_img',
            $model_date_album->id,
            $model_img->id,
        ],
    ]);

    $url_image_delete_list[$id_img] = \xtetis\xdate\Component::makeUrl([
        'path' => [
            \xtetis\xengine\App::getApp()->getAction(),
            'my_img_delete',
            $model_date_album->id,
            $model_img->id,
        ],
    ]);
}

// Урл альбомов
$url_my_albums = \xtetis\xdate\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'my_albums',
    ],
]);

// Урл редактирования моего альбома
$url_edit_my_album = \xtetis\xdate\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'my_album_edit',
        $id,
    ],
]);

$url_date = \xtetis\xdate\Component::makeUrl();

// Ссылка на мой профиль знакомств
$url_my_date_profile = \xtetis\xdate\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'my',
    ],
]);

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage([
    'model_date_album'            => $model_date_album,
    'url_upload_image_to_gallery' => $url_upload_image_to_gallery,
    'url_image_view_list'         => $url_image_view_list,
    'url_my_albums'               => $url_my_albums,
    'url_edit_my_album'           => $url_edit_my_album,
    'model_gallery'               => $model_gallery,
    'url_image_delete_list'       => $url_image_delete_list,
    'url_date'                    => $url_date,
    'url_my_date_profile'         => $url_my_date_profile,
]);
