<?php

/**
 * Добавление альбома в профиль знакомств
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}


if (!\xtetis\xuser\Component::isLoggedIn())
{
    \xtetis\xengine\helpers\LogHelper::customDie('Только для авторизированных пользователей');
}


$id_date_album = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);
$id_img        = \xtetis\xengine\helpers\RequestHelper::get('id2', 'int', 0);


$model_date_album = new \xtetis\xdate\models\DateAlbumModel([
    'id' => $id_date_album,
]);

// Получаем значение по ID
$model_date_album->getById();

// Проверяет - принадлежит ли альбом текущему пользователю
$model_date_album->checkIsOwnerCurrentUser();

// ПОлучаем галерею по ID
$model_gallery = $model_date_album->getModelGallery();

if ($model_date_album->getErrors())
{
    \xtetis\xengine\helpers\LogHelper::customDie($model_date_album->getLastErrorMessage());
}


// Ищем изображение
$current_image_model = false;
foreach ($model_gallery->getImgModelList() as $k => $model_img)
{
    if ($model_img->id == $id_img)
    {
        $current_image_model = $model_img;
    }
}

if (!$current_image_model)
{
    \xtetis\xengine\helpers\LogHelper::customDie('Изображение #' . $id_img . ' не найдено');
}


// Удаляе изображение
if (!$current_image_model->deleteImage())
{
    \xtetis\xengine\helpers\LogHelper::customDie($current_image_model->getLastErrorMessage());
}


$url_my_album = \xtetis\xdate\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'my_album',
        $id_date_album 
    ],
]);

header('Location: ' . $url_my_album);
exit;