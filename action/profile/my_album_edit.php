<?php

/**
 * Добавление альбома в профиль знакомств
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}



$id = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);




if (!\xtetis\xuser\Component::isLoggedIn())
{
    \xtetis\xengine\helpers\LogHelper::customDie('Только для авторизированных пользователей');
}


$model_date_album = new \xtetis\xdate\models\DateAlbumModel([
    'id' => $id,
]);

// Получаем альбюом анкеты знакомств по ID
$model_date_album->getById();

if ($model_date_album->getErrors())
{
    \xtetis\xengine\helpers\LogHelper::customDie($model_date_album->getLastErrorMessage());
}


$model_user = \xtetis\xuser\Component::isLoggedIn();
if (intval($model_user->id) != $model_date_album->id_user)
{
    \xtetis\xengine\helpers\LogHelper::customDie('Альбом не принадлежит Вам');
}

if ($model_date_album->is_main)
{
    \xtetis\xengine\helpers\LogHelper::customDie('Главный альбом нельзя редактировать');
}

$url_validate_edit_album = \xtetis\xdate\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'ajax_validate_my_album_edit',
        $id
    ],
]);


// Урл альбомов
$url_my_albums = \xtetis\xdate\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'my_albums',
    ],
]);

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage([
    'model_date_album'       => $model_date_album,
    'url_validate_edit_album' => $url_validate_edit_album,
    'url_my_albums'          => $url_my_albums,
]);
