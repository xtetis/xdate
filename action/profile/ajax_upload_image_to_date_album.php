<?php

/**
 * Загружаем изображение в альбом пользователя
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$response = [
    'result' => false,
];

if (!\xtetis\xuser\Component::isLoggedIn())
{
    $response['errors']['common'] = 'Только для авторизированных пользователей';

    echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
    exit;
}

// ID альбома
$id = \xtetis\xengine\helpers\RequestHelper::get('id','int',0);

$id_gallery = \xtetis\xengine\helpers\RequestHelper::post('id_gallery', 'int', 0);
$type       = \xtetis\xengine\helpers\RequestHelper::post('type', 'str', '');
$filedata = '';
if ('base64' == $type)
{
    $filedata = \xtetis\xengine\helpers\RequestHelper::post('filedata', 'raw', '');
}

// Проверяем наличие альбома пользователя
$model_date_album = new \xtetis\xdate\models\DateAlbumModel([
    'id' => $id,
]);
$model_date_album->getById();
if ($model_date_album->getErrors())
{
    $response['errors']['common'] = $model_date_album->getLastErrorMessage();

    echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
    exit;
}

if (\xtetis\xuser\Component::isLoggedIn()->id != $model_date_album->id_user)
{
    $response['errors']['common'] = 'Альбом не принадлежит текущему пользователю';

    echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
    exit;
}



if ($model_date_album->id_gallery != $id_gallery)
{
    $response['errors']['common'] = 'Некорректная галерея для альбома пользователя';

    echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
    exit;
}


$img_upload_model = new \xtetis\ximg\models\ImgUploadModel([
    'id_gallery' => $id_gallery,
    'type'       => $type,
    'filedata'   => $filedata,
]);
if (!$img_upload_model->uploadFile())
{
    $response['errors']['common'] = $img_upload_model->getLastErrorMessage();
    echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
    exit;
}




$img_model = new \xtetis\ximg\models\ImgModel([
    'id_gallery' => $id_gallery,
    'filename'   => $img_upload_model->upload_full_path,
]);
if (!$img_model->addImgFromFile())
{

    unlink($img_upload_model->upload_full_path);

    $response['errors']['common'] = $img_model->getLastErrorMessage();
    echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
    exit;
}

$response['result'] = true;



echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
