<?php

/**
 * Альбомы профиля знакомств
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$url_profile_date_album_add = \xtetis\xdate\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'my_album_add',
    ],
]);

if (!\xtetis\xuser\Component::isLoggedIn())
{
    \xtetis\xengine\helpers\LogHelper::customDie('Только для авторизированных пользователей');
}

$model_user = \xtetis\xuser\Component::isLoggedIn();


$model_date_album_list = \xtetis\xdate\models\DateAlbumModel::getDateAlbumModelListByUserId($model_user->id);


// Если не найден ни один альбом 
if (!count($model_date_album_list))
{
    $params = [
        'id_user'                       => $model_user->id,
        'name'                          => 'Главный альбом',
    ];
    
    $model_date_album = new \xtetis\xdate\models\DateAlbumModel($params);
    if (!$model_date_album->addDateAlbum())
    {
        \xtetis\xengine\helpers\LogHelper::customDie($model_date_album->getLastErrorMessage());
    }

    // Заново получаем список альбомов (вместе с уже созданным альбомом)
    $model_date_album_list = \xtetis\xdate\models\DateAlbumModel::getDateAlbumModelListByUserId($model_user->id);
}


$url_list_view_album = [];
foreach ($model_date_album_list as $id_date_album => $model_date_album)
{
    $url_list_view_album[$id_date_album] = \xtetis\xdate\Component::makeUrl([
        'path' => [
            \xtetis\xengine\App::getApp()->getAction(),
            'my_album',
            $id_date_album,
        ],
    ]);
}

// Ссылка на мой профиль знакомств
$url_my_date_profile = \xtetis\xdate\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'my',
    ],
]);

$url_date = \xtetis\xdate\Component::makeUrl();

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage([
    'url_profile_date_album_add' => $url_profile_date_album_add,
    'model_date_album_list'      => $model_date_album_list,
    'url_list_view_album'        => $url_list_view_album,
    'url_my_date_profile'        => $url_my_date_profile,
    'url_date'                   => $url_date,
]);
