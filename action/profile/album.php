<?php

/**
 * Альбом профиля знакомств
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

if (!\xtetis\xuser\Component::isLoggedIn())
{
    \xtetis\xengine\helpers\LogHelper::customDie('Только для авторизированных пользователей');
}


$id = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);

$model_date_album = new \xtetis\xdate\models\DateAlbumModel([
    'id' => $id,
]);

// Получаем значение по ID
$model_date_album->getById();

// ПОлучаем галерею по ID
$model_gallery = $model_date_album->getModelGallery();

if ($model_date_album->getErrors())
{
    \xtetis\xengine\helpers\LogHelper::customDie($model_date_album->getLastErrorMessage());
}

$model_date_profile = new \xtetis\xdate\models\ProfileModel([
    'id_user' => $model_date_album->id_user,
]);

$model_date_profile->getByUserId();
$model_date_profile->getModelUser();

if ($model_date_profile->getErrors())
{
    \xtetis\xengine\helpers\LogHelper::customDie($model_date_profile->getLastErrorMessage());
}

// Ссылка на профиль знакомств
$url_profile = \xtetis\xdate\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'index',
        $model_date_profile->id,
    ],
]);

// Генерируем урлы для просмотра изображений
$url_image_list = [];
$image_src_list = [];
foreach ($model_gallery->getImgModelList() as $id_img => $model_img)
{

    $image_src_list[$id_img] = $model_img->getImgSrc();

    $url_image_list[$id_img] = \xtetis\xdate\Component::makeUrl([
        'path' => [
            \xtetis\xengine\App::getApp()->getAction(),
            'img',
            $model_date_album->id,
            $model_img->id,
        ],
    ]);
}

$url_date = \xtetis\xdate\Component::makeUrl();

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage([
    'image_src_list'     => $image_src_list,
    'url_image_list'     => $url_image_list,
    'model_date_album'   => $model_date_album,
    'url_date'           => $url_date,
    'model_date_profile' => $model_date_profile,
    'url_profile'        => $url_profile,
    /*

    'url_upload_image_to_gallery' => $url_upload_image_to_gallery,
    'url_image_view_list'         => $url_image_view_list,
    'url_my_albums'               => $url_my_albums,
    'url_edit_my_album'           => $url_edit_my_album,
    'model_gallery'               => $model_gallery,
    'url_image_delete_list'       => $url_image_delete_list,

    'url_my_date_profile'         => $url_my_date_profile,
    */
]);
