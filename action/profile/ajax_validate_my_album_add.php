<?php

/**
 * Добавляет альбом профиля знакомств
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$response = [
    'result' => false,
];

if (!\xtetis\xuser\Component::isLoggedIn())
{
    $response['errors']['common'] = 'Только для авторизированных пользователей';

    echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
    exit;
}

$url_profile_date_albums = \xtetis\xdate\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(), 
        'my_albums'
    ],
]);


$name = \xtetis\xengine\helpers\RequestHelper::post('name', 'str', '');

$response['js_success'] = 'xform.goToUrl();';
$response['js_fail']    = '';

$user_model = \xtetis\xuser\Component::isLoggedIn();

$params = [
    'id_user'                       => $user_model->id,
    'name'                          => $name,
];

$model_date_album = new \xtetis\xdate\models\DateAlbumModel($params);
if ($model_date_album->addDateAlbum())
{

    $response['result']            = true;
    $response['data']['go_to_url'] = $url_profile_date_albums;
}
else
{
    $response['errors'] = $model_date_album->getErrors();
}

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
