<?php

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

if (!\xtetis\xuser\Component::isLoggedIn())
{
    \xtetis\xengine\helpers\LogHelper::customDie('Только для авторизированных пользователей');
}

$model_user = \xtetis\xuser\Component::isLoggedIn();

// Профиль пользователя
//-----------------------------------------------------
$date_profile_model = new \xtetis\xdate\models\ProfileModel([
    'id_user' => $model_user->id,
]);
$date_profile_model->getByUserId();
if ($date_profile_model->getErrors())
{
    \xtetis\xengine\helpers\LogHelper::customDie($date_profile_model->getLastErrorMessage());
}
//-----------------------------------------------------


//Урлы
//-----------------------------------------------------
// Урл поиска анкет
$url_date = self::makeUrl();
// Урл редактирования
$url_profile_date_edit = self::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'edit',
    ],
]);
//-----------------------------------------------------

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage([
    'date_profile_model'    => $date_profile_model,
    'url_profile_date_edit' => $url_profile_date_edit,
    'url_date'              => $url_date,
    'model_user'            => $model_user,
]);
