<?php

/**
 * Редактирует альбом профиля знакомств
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$response = [
    'result' => false,
];


$id = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);



$model_date_album = new \xtetis\xdate\models\DateAlbumModel([
    'id' => $id,
]);

// Получаем альбюом анкеты знакомств по ID
$model_date_album->getById();

// Проверяет - принадлежит ли альбом текущему пользователю
$model_date_album->checkIsOwnerCurrentUser();

// Проверяет, что текущий альюом не является главным
$model_date_album->checkIsNotMain();

if ($model_date_album->getErrors())
{
    $response['errors']['common'] = $model_date_album->getLastErrorMessage();

    echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
    exit;
}




$url_profile_date_albums = \xtetis\xdate\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(), 
        'my_albums'
    ],
]);



$name = \xtetis\xengine\helpers\RequestHelper::post('name', 'str', '');

$response['js_success'] = 'xform.goToUrl();';
$response['js_fail']    = '';


// Для найденной модели проставляем имя
$model_date_album->name = $name;

// Редактируем имя альбома
if ($model_date_album->editDateAlbumName())
{

    $response['result']            = true;
    $response['data']['go_to_url'] = $url_profile_date_albums;
}
else
{
    $response['errors'] = $model_date_album->getErrors();
}

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
