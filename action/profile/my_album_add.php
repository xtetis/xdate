<?php

/**
 * Добавление альбома в профиль знакомств
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$url_validate_add_album = \xtetis\xdate\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'ajax_validate_my_album_add'
    ],
]);

if (!\xtetis\xuser\Component::isLoggedIn())
{
    \xtetis\xengine\helpers\LogHelper::customDie('Только для авторизированных пользователей');
}






$model_date_album = new \xtetis\xdate\models\DateAlbumModel();

// Урл альбомов
$url_my_albums = \xtetis\xdate\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'my_albums',
    ],
]);

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage([
    'model_date_album'       => $model_date_album,
    'url_validate_add_album' => $url_validate_add_album,
    'url_my_albums'          => $url_my_albums,
]);
