<?php

/**
 * Альбом профиля знакомств
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

if (!\xtetis\xuser\Component::isLoggedIn())
{
    \xtetis\xengine\helpers\LogHelper::customDie('Только для авторизированных пользователей');
}

$id_date_album = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);
$id_img        = \xtetis\xengine\helpers\RequestHelper::get('id2', 'int', 0);

$model_date_album = new \xtetis\xdate\models\DateAlbumModel([
    'id' => $id_date_album,
]);

// Получаем значение по ID
$model_date_album->getById();

// ПОлучаем галерею по ID
$model_gallery = $model_date_album->getModelGallery();

if ($model_date_album->getErrors())
{
    \xtetis\xengine\helpers\LogHelper::customDie($model_date_album->getLastErrorMessage());
}

$current_image_model = false;
foreach ($model_gallery->getImgModelList() as $k => $model_img)
{
    if ($model_img->id == $id_img)
    {
        $current_image_model = $model_img;
    }
}

if (!$current_image_model)
{
    \xtetis\xengine\helpers\LogHelper::customDie('Изображение #' . $id_img . ' не найдено');
}

// Урл альбома
$url_album = \xtetis\xdate\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'album',
        $model_date_album->id,
    ],
]);

// ---------------------------------------------

$model_date_profile = new \xtetis\xdate\models\ProfileModel([
    'id_user' => $model_date_album->id_user,
]);

$model_date_profile->getByUserId();
$model_date_profile->getModelUser();

if ($model_date_profile->getErrors())
{
    \xtetis\xengine\helpers\LogHelper::customDie($model_date_profile->getLastErrorMessage());
}

// Ссылка на профиль знакомств
$url_profile = \xtetis\xdate\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'index',
        $model_date_profile->id,
    ],
]);
// ------------------------------------------

// Урл поиска анкет
$url_date = \xtetis\xdate\Component::makeUrl();

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage([
    'model_date_album'    => $model_date_album,
    'current_image_model' => $current_image_model,
    'url_album'           => $url_album,
    'url_profile'         => $url_profile,
    'url_date'            => $url_date,
    'model_date_profile'  => $model_date_profile,
]);
