<?php

/**
 * Рендер списка профилей
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}



$id_date_profile = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);

$model_date_profile = new \xtetis\xdate\models\ProfileModel(
    [
        'id' => intval($id_date_profile),
    ]
);

$model_date_profile->getById();


if ($model_date_profile->getErrors())
{
    \xtetis\xengine\helpers\LogHelper::customDie($model_date_profile->getLastErrorMessage());
}

if (!intval($model_date_profile->id_profile_type))
{
    \xtetis\xengine\helpers\LogHelper::customDie('Тип профиля анкеты не заполнен');
}


$model_date_profile->getModelUser();

if ($model_date_profile->getErrors())
{
    \xtetis\xengine\helpers\LogHelper::customDie($model_date_profile->getLastErrorMessage());
}


$model_user_current = \xtetis\xuser\Component::isLoggedIn();
if ($model_user_current)
{

    // Если это профиль текущего пользователя
    if ($model_user_current->id == $model_date_profile->id_user)
    {
        
        $url_my_date_profile = \xtetis\xdate\Component::makeUrl([
            'path' => [
                \xtetis\xengine\App::getApp()->getAction(),
                'my',
            ],
        ]);

        header('Location: ' . $url_my_date_profile);
        exit;
    }
}



// Урл поиска анкет
$url_date = \xtetis\xdate\Component::makeUrl();

// Если нет главного изображения
$profile_img_src = \xtetis\ximg\models\ImgModel::getNoimageSrc();



$model_date_album_list = \xtetis\xdate\models\DateAlbumModel::getDateAlbumModelListByUserId($model_date_profile->id_user);

// Главное изображение
foreach ($model_date_album_list as $id_date_album => $model_date_album)
{
    if (intval($model_date_album->is_main))
    {
        $model_date_album->getModelGallery();
        if (!$model_date_album->getErrors())
        {
            $model_gallery = $model_date_album->model_gallery;
            $model_img     = $model_gallery->getMainImgModel();

            if ($model_img)
            {
                $profile_img_src = $model_img->getImgSrc();
            }
        }
    }
}


// Урлы альбомов пользователя
$url_list_view_album = [];
foreach ($model_date_album_list as $id_date_album => $model_date_album)
{
    $url_list_view_album[$id_date_album] = \xtetis\xdate\Component::makeUrl([
        'path' => [
            \xtetis\xengine\App::getApp()->getAction(),
            'album',
            $id_date_album,
        ],
    ]);
}

\xtetis\xengine\App::getApp()->setParam('layout', 'list');

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage([
    'model_date_profile'    => $model_date_profile,
    'url_date'              => $url_date,
    'profile_img_src'       => $profile_img_src,
    'model_date_album_list' => $model_date_album_list,
    'url_list_view_album'   => $url_list_view_album,
]);
