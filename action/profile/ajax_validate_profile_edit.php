<?php

/**
 * Сохраняет изменения в профиле знакомств
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$url_profile_date = \xtetis\xdate\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'my',
    ],
]);

if (!\xtetis\xuser\Component::isLoggedIn())
{
    $response['errors']['common'] = 'Только для авторизированных пользователей';

    echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
    exit;
}


$response = [
    'result' => false,
];

$name = \xtetis\xengine\helpers\RequestHelper::post('name', 'str', '');


$response['js_success'] = 'xform.goToUrl();';
$response['js_fail']    = '';



$date_profile_model = new \xtetis\xdate\models\ProfileModel();
$date_profile_model->loadPostData([
    'birth_date' => [
        'type'    => 'str',
        'default' => '',
    ],
    'id_profile_type' => [
        'type'    => 'int',
        'default' => 0,
    ],
    /*
    'id_city' => [
        'type'    => 'int',
        'default' => 0,
    ],
    */
    'id_geo_object' => [
        'type'    => 'int',
        'default' => 0,
    ],
    'hide_images_noauth' => [
        'type'    => 'int',
        'default' => 0,
    ],
    'about' => [
        'type'    => 'str',
        'default' => '',
    ],
    'height' => [
        'type'    => 'int',
        'default' => 0,
    ],
    'weight' => [
        'type'    => 'int',
        'default' => 0,
    ],
    
]);

if ($date_profile_model->saveDateProfile())
{
    $date_profile_model->model_user->name = $name;
    if (!$date_profile_model->model_user->updateName())
    {
        $response['errors'] = $date_profile_model->model_user->getErrors();
    }
    else
    {
        $response['result']            = true;
        $response['data']['go_to_url'] = $url_profile_date;
    }
}
else
{
    $response['errors'] = $date_profile_model->getErrors();
}

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
