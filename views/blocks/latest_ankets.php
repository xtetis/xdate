<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }
?>



<div class="card-columns">

    <?php foreach ($model_data_profile_list->model_date_profile_list as $id_profile => $date_profile_model): ?>
        <?=\xtetis\xdate\Component::renderBlock(
            'blocks/profile_list_item',
            [
                'date_profile_model'=>$date_profile_model,
            ]
        ); 
        ?>
    <?php endforeach;?>

</div>
