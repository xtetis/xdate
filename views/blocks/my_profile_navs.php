<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }


    $url_query_list = [
        'my'=>'Мой профиль знакомств',
        'my_albums' =>'Мои альбомы',
    ];

    $url_list = [];
    $url_selected = [];
    foreach ($url_query_list as $url_query=>$url_text) 
    {
        $url_list[$url_query] = \xtetis\xdate\Component::makeUrl([
            'path' => [
                \xtetis\xengine\App::getApp()->getAction(),
                $url_query,
            ],
        ]);

        $url_selected[$url_query] = (\xtetis\xengine\App::getApp()->getQuery() == $url_query);
    }
?>
<ul class="nav nav-pills mb-3">
    <?php foreach ($url_query_list as $url_query=>$url_text): ?>
    <li class="nav-item">
        <a class="nav-link <?=($url_selected[$url_query])?' active':''?>"
           href="<?=$url_list[$url_query]?>">
           <?=$url_text?>
        </a>
    </li>
    <?php endforeach; ?>
</ul>
