<?php

    /**
     * Элемент списка анкет знакомств
     */

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }
?>




<div class="card">
    <a href="<?=$date_profile_model->getLink()?>">
        <img src="<?=$date_profile_model->getProfileMainImgSrc()?>"
                class="img-fluid card-img-top"
                srcset=""
                alt="Фото профиля <?=$date_profile_model->getModelUser()->getUserLoginOrName()?> из <?=$date_profile_model->getProfileLocationText()?>">
    </a>
    <div class="card-header text-center">
        <a href="<?=$date_profile_model->getLink()?>">
            <h3>
                <?=$date_profile_model->getModelUser()->getUserLoginOrName()?>
            </h3>
            <div class="text-center">
                <?=$date_profile_model->getTypeProfileText()?>
            </div>
            <div style="font-size:1.5rem;">
                <?=$date_profile_model->getProfileAgeYearsText()?>
                <?=$date_profile_model->getAgeYearsSuffix()?>
            </div>
        </a>
    </div>
    <div class="card-body pt-0">
        <div class="text-center"
                style="    font-size: 20px;">
            <a href="<?=$date_profile_model->getLinkSearchLocation()?>">
                <?=$date_profile_model->getProfileLocationPartText(1)?>
            </a>
        </div>
    </div>
</div>

