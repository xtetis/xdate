<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    // Тип профиля
    //*****************************************************
    /*
    $date_profile_type_model                         = new \xtetis\xdate\models\ProfileTypeModel();
    $date_profile_type_model->text_nooption_sekected = 'Любой тип';
    $date_profile_type_options                       = $date_profile_type_model->getOptions();
    */
    //*****************************************************

    $url_profile_search_validate_form = \xtetis\xdate\Component::makeUrl([
        'path' => [
            \xtetis\xengine\App::getApp()->getAction(),
            'ajax_profile_search_validate_form',
        ],
    ]);

    $urls['xgeo_multiple_data_url'] = \xtetis\xgeo\Component::makeUrl([
        'path'  => [
            'data',
            'ajax_js_tree',
        ],
        'query' => [
            'all_tree_selectable'  => 1,
        ],
    ]);

?>


<?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => $url_profile_search_validate_form,
    'form_type'    => 'ajax',
    'form_id'      => 'profile_list_search_form',
]);?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4>
                    Параметры поиска
                </h4>
            </div>

            <div class="card-body">
                <div class="profile_select_form_container">
                    <div class="profile_select_form_inner">
                        <div class="row">
                            <div class="col-md-4">

                                <?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Тип анкеты',
            'name'    => 'id_profile_type',
            'options' => (new \xtetis\xdate\models\ProfileTypeModel())->getOptions(),
        ],
        'value'      => $model_data_profile_list->id_profile_type,
    ]
)?>
                            </div>
                            <div class="col-md-4">
                                <?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Возраст от',
            'name'    => 'age_start',
            'options' => $model_data_profile_list->getAgeOptions(),
        ],
        'value'      => $model_data_profile_list->age_start,
    ]
)?>
                            </div>

                            <div class="col-md-4">
                                <?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Возраст до',
            'name'    => 'age_end',
            'options' => $model_data_profile_list->getAgeOptions(),
        ],
        'value'      => $model_data_profile_list->age_end,
    ]
)?>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">

                                <?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select_multilevel',
        'attributes' => [
            'label'         => 'Населенный пункт',
            'name'          => 'id_location',
            'selected_text' => $model_data_profile_list->getSearchLocationText(),
            'placeholder'   => 'Выберите локацию',
            'data_url'      => $urls['xgeo_multiple_data_url'],
        ],
        'value'      => $model_data_profile_list->id_location,
    ]
)?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-center pt-0">
                <div class="row">
                    <div class="col-md-6">
                    <a href="<?=$urls['url_date']?>"
                        class="btn  btn-secondary"
                        style="width: 100%;">Сбросить</a>
                    </div>
                    <div class="col-md-6">
                    <button type="submit"
                        class="btn  btn-primary"
                        style="width: 100%;">Поиск</button>
                    </div>
                </div>


            </div>
        </div>

    </div>
</div>
</div>
<?=\xtetis\xform\Component::renderFormEnd();?>
