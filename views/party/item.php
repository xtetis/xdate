<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }


    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        
        [
            'url'  =>  $urls['url_date'],
            'name' => 'Знакомства',
        ],
        
        [
            'url'  =>  $urls['url_date_party'],
            'name' => 'Вечеринки',
        ],
        
        [
            'name' => $model_party->name.' - '.$model_party->party_date,
        ],
    ]);



    // Устанавливаем Title страницы
    \xtetis\xengine\helpers\SeoHelper::setTitle(
        $model_party->name.' - '.$model_party->party_date.' и другие свинг вечеринки '.' - '.APP_NAME
        );

?>


<h1><?=$model_party->name?> - <?=$model_party->party_date?></h1>
<div class="">
    <?php if ($model_gallery_cover):?>
    <div class="lbox_gallery">
        <?php foreach ($model_gallery_cover->getImgModelList() as $id_img => $model_image):?>
        <a href="<?=$model_image->getImgSrc()?>"
           data-lightbox="roadtrip">
            <img src="<?=$model_image->getImgSrc()?>"
                 style="max-width:150px; max-height:150px;"
                 alt="<?=$model_party->name?> #<?=$id_img?>" />
        </a>

        <?php endforeach; ?>
    </div>
    <?php endif; ?>
    <div class="">
        <?=$model_party->about?>
    </div>
    <?php if ($model_gallery_place):?>
    <br>
    <div class="">
        <h4>
            Место проведения вечеринки
        </h4>
    </div>
    <div class="lbox_gallery">
        <?php foreach ($model_gallery_place->getImgModelList() as $id_img => $model_image):?>
        <a href="<?=$model_image->getImgSrc()?>"
           data-lightbox="roadtrip">
            <img src="<?=$model_image->getImgSrc()?>"
                 style="max-width:150px; max-height:150px;"
                 alt="<?=$model_party->name?> #<?=$id_img?>" />
        </a>

        <?php endforeach; ?>
    </div>
    <?php endif; ?>
    <div class="">
        <h4>
            О вечеринке
        </h4>
    </div>
    <div>
        <div class="table-responsive">
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <td>
                            Дата начала
                        </td>
                        <td>
                            <?=$model_party->party_date?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Локация
                        </td>
                        <td>
                            <?=$model_party->getLocationText()?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Автор
                        </td>
                        <td>
                            <a class="btn  btn-warning" href="<?=$model_date_profile_author->getLink()?>">
                                <?=$model_user_author->getUserLoginOrName()?>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
