<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'name' => 'Знакомства',
            'url'  => $urls['url_date'],
        ],
        [
            'name' => 'Вечеринки',
            'url'  => $urls['url_party'],
        ],
        [
            'name' => 'Добавление анонса вечеринки',
        ],
    ]);

    // Устанавливаем Title страницы
    \xtetis\xengine\helpers\SeoHelper::setTitle('Добавление анонса вечеринки - ' . APP_NAME);
?>
<h3>
    Добавление вечеринки
</h3>

<br>

<?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => $urls['validate_form'],
    'form_type'    => 'ajax',
]);?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select_multilevel',
        'attributes' => [
            'label'         => 'Город проведения вечеринки',
            'name'          => 'id_city',
            'selected_text' => $model_date_party->getPartyLocationText(),
            'placeholder'   => 'Выберите город',
            'data_url'      => $urls['xgeo_multiple_data_url'],
        ],
        'value'      => $model_date_party->id_city,
    ]
)?>




<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label' => 'Название вечеринки',
            'name'  => 'name',
        ],
        'value'      => $model_date_party->name,
    ]
)?>


<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'textarea',
        'attributes' => [
            'label' => 'Описание вечеринки',
            'name'  => 'about',
            'class' => 'tinymce form-control',

        ],
        'value'      => $model_date_party->about,
    ]
)?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label'    => 'Дата проведения вечеринки',
            'name'     => 'party_date',
            'class'    => 'date-control form-control',
            'readonly' => 'readonly',
        ],
        'value'      => $model_date_party->party_date,
    ]
)?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_image_noaulbum',
        'attributes' => [
            'label'      => 'Обложка вечеринки',
            'name'       => 'image_cover',
            'max_images' => 1,
        ],
    ]
)?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_image_noaulbum',
        'attributes' => [
            'label'      => 'Фото места, где будет вечеринка',
            'name'       => 'image_party_place',
            'max_images' => 5,
        ],
    ]
)?>


<button type="submit"
        class="btn btn-block btn-primary mb-4">Сохранить</button>
<?=\xtetis\xform\Component::renderFormEnd();?>

