<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'name' => 'Знакомства',
            'url'=>$urls['url_date'] ,
        ],
        [
            'name' => 'Вечеринки'
        ],
    ]);

    // Устанавливаем Title страницы
    \xtetis\xengine\helpers\SeoHelper::setTitle('Свинг вечеринки - ' . APP_NAME);

    // Устанавливаем Description страницы
    \xtetis\xengine\helpers\SeoHelper::setDescription(
        'Анонсы свинг вечеринок на '.
        APP_NAME.
        '. Описания мероприятий, отчеты о проведенных мероприятиях и многое другое.'
    );
?>


<h3>
    Анонсы вечеринок
</h3>

<?php if (\xtetis\xuser\Component::isLoggedIn()): ?>
<div class="mb-2">
    <a href="<?=$urls['url_party_add'] ?>"
       style="    width: 100%;"
       class="btn  btn-primary">Добавить анонс вечеринки</a>
</div>
<?php endif; ?>

<div class="card-columns">

    <?php foreach ($model_date_party_list->model_list as $id => $model_date_party): ?>


    <div class="card">
        <a href="<?=$model_date_party->getLinkToParty()?>">
            <img src="<?=$model_date_party->getMainImgSrc()?>"
                 class="img-fluid card-img-top"
                 srcset="">
        </a>
        <div class="card-header text-center pt-0 pb-0">
            <a href="<?=$model_date_party->getLinkToParty()?>">
                <h3>
                    <?=$model_date_party->name?>
                </h3>
            </a>
        </div>
        <div class="card-body">
            <div style="font-size:1.3rem;"
                 class="text-center">
                Автор вечеринки
            </div>
            <div style="font-size:1.2rem;"
                 class="text-center">
                <?=$model_date_party->getModelUser()->getUserLoginOrName()?>
            </div>
            <br>
            <div class="text-center"
                 style="    font-size: 20px;">
                Место проведения
            </div>
            <div class="text-center"
                 style="    font-size: 16px;">
                <?=$model_date_party->getLocationText()?>
            </div>
            <br>
            <div class="text-center"
                 style="    font-size: 20px;">
                Дата проведения
            </div>
            <div class="text-center"
                 style="    font-size: 16px;">
                <?=$model_date_party->party_date?>
            </div>
        </div>
    </div>

    <?php endforeach;?>

</div>
<div class="pagination_container">
    <?=$paginate?>
</div>
