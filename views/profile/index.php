<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $url_date,
            'name' => 'Знакомства',
        ],
        [
            'name' => '' . $model_date_profile->getModelUser()->getUserLoginOrName() . ' ' . $model_date_profile->getProfileAgeYearsText() . ' ' . $model_date_profile->getAgeYearsSuffix() . ' - профиль знакомств',
        ],
    ]);

    // Устанавливаем Title страницы
    \xtetis\xengine\helpers\SeoHelper::setTitle(
        '' .
        $model_date_profile->getModelUser()->getUserLoginOrName() .
        ' ' .
        $model_date_profile->getProfileAgeYearsText() .
        ' ' .
        $model_date_profile->getAgeYearsSuffix() .
        ' из ' .
        morphos\Russian\GeographicalNamesInflection::getCase(
            $model_date_profile->getProfileLocationText(),
            'родительный'
        ) .
        ' - свинг знакомства ' .
        APP_NAME);

    // Устанавливаем Description страницы
    \xtetis\xengine\helpers\SeoHelper::setDescription(
        $model_date_profile->getTypeProfileText() .
        ' из ' .
        morphos\Russian\GeographicalNamesInflection::getCase(
            $model_date_profile->getProfileLocationText(),
            'родительный'
        ) .
        ', ' .
        $model_date_profile->getModelUser()->getUserLoginOrName() .
        ' ' .
        $model_date_profile->getProfileAgeYearsText() .
        ' ' .
        $model_date_profile->getAgeYearsSuffix() .
        ' - анкета знакомств свингеров ' .
        morphos\Russian\GeographicalNamesInflection::getCase(
            $model_date_profile->getProfileLocationText(),
            'родительный'
        )
    );

?>
<h3>
    <?=$model_date_profile->getModelUser()->getUserLoginOrName()?>
</h3>

<div class="row">

    <div class="col-md-6 col-xl-4">
        <div class="card mb-3">
            <img class="img-fluid card-img-top"
                 src="<?=$profile_img_src?>"
                 alt="Card image cap">
            <div class="card-body">
                <?=\xtetis\xmessage\Component::getButtonSendMessage($model_date_profile->id_user)?>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xl-8">
        <div class="card mb-3">
            <div class="card-body">

                <div class="table-responsive">
                    <table class="table table-striped  table-bordered">
                        <tbody>
                            <tr>
                                <td>Анкета создана</td>
                                <td>
                                    <?=date('d.m.Y', strtotime($model_date_profile->create_date))?>
                                </td>
                            </tr>
                            <tr>
                                <td>Последняя авторизация</td>
                                <td>
                                    <?=$model_date_profile->getModelUser()->getLastAccessFormatted()?>
                                </td>
                            </tr>
                            <tr>
                                <td>Возраст</td>
                                <td>
                                    <?=$model_date_profile->getProfileAgeYearsText()?>
                                    <?=$model_date_profile->getAgeYearsSuffix()?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Локация
                                </td>
                                <td>
                                    <a href="<?=$model_date_profile->getLinkSearchLocation()?>">
                                        <?=$model_date_profile->getProfileLocationFullText();?>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Тип анкеты
                                </td>
                                <td>
                                    <?=$model_date_profile->getTypeProfileText();?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    О себе
                                </td>
                                <td style="white-space: normal;">
                                    <?=$model_date_profile->getAboutText();?>
                                </td>
                            </tr>
                            <tr>
                                <td>Рост</td>
                                <td>
                                    <?=$model_date_profile->getHeightText();?>
                                </td>
                            </tr>
                            <tr>
                                <td>Вес</td>
                                <td>
                                    <?=$model_date_profile->getWeightText();?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="card mb-3">
            <div class="card-body  mt-3">

                <?php foreach ($model_date_album_list as $id_date_album => $model_date_album): ?>
                <div>
                    <a href="<?=$url_list_view_album[$id_date_album]?>">
                        <div class="alert <?=(intval($model_date_album->is_main) ? 'alert-warning' : 'alert-info')?> "
                             role="alert">
                            <?=$model_date_album->name?>
                        </div>
                    </a>
                </div>
                <?php endforeach;?>

            </div>
        </div>
    </div>

</div>

<div class="card">
    <div class="card-body">
        <h5 class="card-title">Посмотрите другие анкеты</h5>
        <div>
<?=\xtetis\xdate\Component::getBlockAnketsRandom([
    'where_arr' => [
        'id NOT IN (' . $model_date_profile->id . ')',
    ],
    'limit'     => 3,
])?>
        </div>
    </div>
</div>
