<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'name' => 'Мои альбомы',
            'url'  => $url_my_albums,
        ],
        [
            'name' => 'Добавление альбома',
        ],
    ]);
?>
<h3>
    Создание альбома профиля
</h3>

<br>

<?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => $url_validate_add_album,
    'form_type'    => 'ajax',
]);?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label' => 'Имя альбома',
            'name'  => 'name',
            'class' => ' form-control',
        ],
        'value'      => $model_date_album->name,
    ]
)?>


<button type="submit"
        class="btn btn-block btn-primary mb-4">Сохранить</button>
<?=\xtetis\xform\Component::renderFormEnd();?>

