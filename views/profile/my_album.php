<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $url_date,
            'name' => 'Знакомства',
        ],
        [
            'url'  => $url_my_date_profile,
            'name' => 'Мой профиль',
        ],
        [
            'name' => 'Мои альбомы',
            'url'  => $url_my_albums,
        ],
        [
            'name' => $model_date_album->name,
        ],
    ]);

?>

<?php if (!$model_date_album->is_main): ?>
<div class=""
     style="float: right;">
    <a href="<?=$url_edit_my_album?>"
       class="btn  btn-success">
        Редактировать
    </a>
</div>
<?php endif;?>
<h3>
    <?=$model_date_album->name?>
</h3>


<div class="text-left">
    <?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => '/',
    'form_type'    => 'ajax',
]);?>
    <h5>Добавление изображения</h5>





    <?=\xtetis\xform\Component::renderField([
    'template'   => 'input_image',
    'attributes' => [
        'url_upload_image_to_gallery' => $url_upload_image_to_gallery,
    ],
    'value'      => $model_date_album->id_gallery,
])?>


    <?=\xtetis\xform\Component::renderFormEnd();?>
</div>





<div class="p-3 gallery_container gallery_<?=$model_date_album->id_gallery?>_container">
    <div class="gallery_<?=$model_date_album->id_gallery?>_container_inner">
        <div class="card-columns">
            <?php foreach ($model_gallery->getImgModelList() as $id_img => $model_img): ?>
            <div class="card"
                 style="box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.5);">
                <a href="<?=$url_image_view_list[$id_img]?>">
                    <img class="img-fluid card-img-top"
                         src="<?=$model_img->getImgSrc()?>"
                         alt="Card image cap">
                </a>
                <div class="card-body">
                    <div class="text-center">
                        <a href="<?=$url_image_delete_list[$id_img]?>"
                           onclick="return confirm('Вы хотите удалить изображение #<?=$id_img?>');"
                           class="btn  btn-danger">
                            Удалить
                        </a>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
        </div>
    </div>
</div>
