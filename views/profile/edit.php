<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'name' => 'Профиль знакомств',
            'url'  => $urls['url_my_date_profile'],
        ],
        [
            'name' => 'Редактирование профиля знакомств',
        ],
    ]);

    // Добавляем файл JS для обработки страницы
    //\xengine\helpers\StatResHelper::addTemplateJs(__DIR__ . '/../../web/js/xdate.js');

    $birth_date = '';
    if (strlen(strval($model_date_profile->birth_date)))
    {
        $birth_date = date('d.m.Y', strtotime(strval($model_date_profile->birth_date)));
    }

    // Устанавливаем Title страницы
    \xtetis\xengine\helpers\SeoHelper::setTitle('Редактирование профиля знакомств - '.APP_NAME);

?>
<h3>
    Редактирование профиля знакомств
</h3>

<br>

<?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => $urls['url_validate_profile_edit'],
    'form_type'    => 'ajax',
    'form_id'      => 'form__my_profile_edit',
]);?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label' => 'Имя',
            'name'  => 'name',
            'class' => ' form-control',
        ],
        'value'      => $model_user->name,
    ]
)?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label'    => 'Дата рождения',
            'name'     => 'birth_date',
            'class'    => 'date-control form-control',
            'readonly' => 'readonly',
        ],
        'value'      => $birth_date,
    ]
)?>



<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Тип анкеты',
            'name'    => 'id_profile_type',
            'options' => $date_profile_type_options,
        ],
        'value'      => $model_date_profile->id_profile_type,
    ]
)?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select_multilevel',
        'attributes' => [
            'label'         => 'Населенный пункт',
            'name'          => 'id_geo_object',
            'selected_text' => $model_date_profile->getProfileLocationFullText(),
            'placeholder'   => 'Выберите населенный пункт',
            'data_url'      => $urls['xgeo_multiple_data_url'],
            'level'         => 3,
        ],
        'value'      => $model_date_profile->id_city,
    ]
)?>

<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Скрывать изображения в альбомах для неавторизироваанных пользователей',
            'name'    => 'hide_images_noauth',
            'options' => [
                0=> 'Не скрывать изображения',
                1=> 'Скрывать изображения'
            ],
        ],
        'value'      => '',
    ]
)?>



<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'textarea',
        'attributes' => [
            'label' => 'О себе	',
            'name'  => 'about',
            'style' => 'min-height: 150px;height: 110px;',

        ],
        'value'      => $model_date_profile->about,
    ]
)?>





<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Рост (см)',
            'name'    => 'height',
            'options' => \xtetis\xdate\models\ProfileModel::getHeightOptions(true),
        ],
        'value'      => $model_date_profile->height,
    ]
)?>


<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Вес (кг)',
            'name'    => 'weight',
            'options' => \xtetis\xdate\models\ProfileModel::getWeightOptions(true),
        ],
        'value'      => $model_date_profile->weight,
    ]
)?>


<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'select',
        'attributes' => [
            'label'   => 'Ориентация',
            'name'    => 'orientation',
            'options' => \xtetis\xdate\models\ProfileModel::getOrientationOptions(true),
        ],
        'value'      => $model_date_profile->orientation,
    ]
)?>


<button type="submit"
        class="btn btn-block btn-primary mb-4">Сохранить</button>
<?=\xtetis\xform\Component::renderFormEnd();?>
