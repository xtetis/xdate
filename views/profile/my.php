<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }
    

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $url_date,
            'name' => 'Знакомства',
        ],
        [
            'name' => 'Мой профиль',
        ],
    ]);


    // Устанавливаем Title страницы
    \xtetis\xengine\helpers\SeoHelper::setTitle('Мой профиль знакомств - '.APP_NAME
        );

?>

<script>
    sfsdfsdfsdfd
</script>

<?=\xtetis\xdate\Component::renderBlock('blocks/my_profile_navs')?>

<br>
<a href="<?=$url_profile_date_edit?>"
   style="    float: right;"
   class="btn  btn-success">Редактировать профиль</a>
<div class="table-responsive">
    <table class="table table-striped">
        <tbody>
            <tr>
                <th>Имя</th>
                <td><?=$model_user->getUserLoginOrName()?></td>
            </tr>
            <tr>
                <th>Дата рождения</th>
                <td><?=isset($date_profile_model->birth_date)?date('d.m.Y',strtotime($date_profile_model->birth_date)):'Не указано'?>
                </td>
            </tr>
            <tr>
                <th>Тип профиля</th>
                <td><?=$date_profile_model->getTypeProfileText()?></td>
            </tr>
            <tr>
                <th>Локация</th>
                <td><?=$date_profile_model->getProfileLocationFullText()?></td>
            </tr>
            <tr>
                <th>О себе</th>
                <td style="white-space: normal;">
                    <?=$date_profile_model->getAboutText();?>
                </td>
            </tr>
            <tr>
                <th>Рост</th>
                <td>
                    <?=$date_profile_model->getHeightText();?>
                </td>
            </tr>
            <tr>
                <th>Вес</th>
                <td>
                    <?=$date_profile_model->getWeightText();?>
                </td>
            </tr>
        </tbody>
    </table>
</div>
