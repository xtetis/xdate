<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $url_date,
            'name' => 'Знакомства',
        ],
        [
            'url'  => $url_my_date_profile,
            'name' => 'Мой профиль',
        ],
        [
            'name' => 'Мои альбомы',
        ],
    ]);


    // Устанавливаем Title страницы
    \xtetis\xengine\helpers\SeoHelper::setTitle(
        'Мои альбомы - '.APP_NAME
        );

?>

<?=\xtetis\xdate\Component::renderBlock('blocks/my_profile_navs')?>


<a href="<?=$url_profile_date_album_add?>"
   style="    float: right;     margin-top: -45px;"
   class="btn  btn-success">Добавить альбом</a>

<div>
    <?php foreach ($model_date_album_list as $id_date_album => $model_date_album): ?>
    <div>
        <a href="<?=$url_list_view_album[$id_date_album]?>">
            <div class="alert <?=(intval($model_date_album->is_main) ? 'alert-warning' : 'alert-info')?> "
                 role="alert">
                <?=$model_date_album->name?>
            </div>
        </a>
    </div>
    <?php endforeach;?>
</div>
