<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'name' => 'Мои альбомы',
            'url'  => $url_my_albums,
        ],
        [
            'name' => $model_date_album->name,
            'url'  => $url_my_album,
        ],
        [
            'name' => 'Изображение #' . $current_image_model->id,
        ],
    ]);

?>
<h3>
    <?=$model_date_album->name?>, фото #<?=$current_image_model->id?>
</h3>




<div class="card"
     style="box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.5);">
    <img class="img-fluid card-img-top"
         src="<?=$current_image_model->getImgSrc()?>"
         alt="Card image cap">
</div>
