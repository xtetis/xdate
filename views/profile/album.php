<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'url'  => $url_date,
            'name' => 'Знакомства',
        ],
        
        [
            'url'  => $url_profile,
            'name' => 'Профиль '.$model_date_profile->getModelUser()->getUserLoginOrName(),
        ],
        [
            'name' => $model_date_album->name,
        ],
    ]);

    // Устанавливаем Title страницы
    \xtetis\xengine\helpers\SeoHelper::setTitle('Альбом знакомств '.$model_date_album->name.' пользователя '.$model_date_profile->getModelUser()->getUserLoginOrName().' - '.APP_NAME);

    
?>


<h3>
    <?=$model_date_album->name?>
</h3>







<div class="p-3 gallery_container gallery_<?=$model_date_album->id_gallery?>_container">
    <div class="gallery_<?=$model_date_album->id_gallery?>_container_inner">
        <div class="card-columns">
            <?php foreach ($image_src_list as $id_img => $img_src): ?>
            <div class="card"
                 style="box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.5);">
                <a href="<?=$url_image_list[$id_img]?>">
                    <img class="img-fluid card-img-top"
                         src="<?=$img_src?>"
                         alt="Card image cap">
                </a>
            </div>
            <?php endforeach;?>
        </div>
    </div>
</div>
