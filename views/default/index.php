<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    $h1 = 'Свинг знакомства';
    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'name' => 'Знакомства',
        ],
    ]);
    $h2 = '';

    // Устанавливаем Description страницы
    \xtetis\xengine\helpers\SeoHelper::setDescription(
        'Познакомиться ' .
        ' онлайн бесплатно. Найдите пару или партнера '.
        ' для свинга. Все свингеры '.
        ' знакомятся на '.APP_NAME.'.',
        true
    );

    if ($model_data_profile_list->model_geo_object)
    {
        $h1 .= ' в ' .
        morphos\Russian\GeographicalNamesInflection::getCase($model_data_profile_list->model_geo_object->name, 'prepositional');

        \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
            [
                'url'  => $urls['url_date'],
                'name' => 'Знакомства',
            ],
            [
                'name' => $h1,
            ],
        ]);

        // Устанавливаем Description страницы
        \xtetis\xengine\helpers\SeoHelper::setDescription(
            'Познакомиться в ' .
            morphos\Russian\GeographicalNamesInflection::getCase($model_data_profile_list->model_geo_object->name, 'prepositional').
            ' онлайн бесплатно. Найдите пару или партнера в '.
            morphos\Russian\GeographicalNamesInflection::getCase($model_data_profile_list->model_geo_object->name, 'prepositional').
            ' для свинга. Все свингеры '.
            morphos\Russian\GeographicalNamesInflection::getCase($model_data_profile_list->model_geo_object->name, 'genitive').
            ' знакомятся на '.APP_NAME.'.',
            true
        );

        $parent_geo = $model_data_profile_list->model_geo_object->getParentModel();
        if ($parent_geo)
        {
            $h2 = 'Свинг знакомства в '.
            morphos\Russian\GeographicalNamesInflection::getCase(
                $model_data_profile_list->model_geo_object->name
                , 
                'prepositional'
            ).
            ' '.
            morphos\Russian\GeographicalNamesInflection::getCase(
                $parent_geo->name
                , 
                'prepositional'
            );

            // Устанавливаем Description страницы
            \xtetis\xengine\helpers\SeoHelper::setDescription(
                'Познакомиться в ' .
                morphos\Russian\GeographicalNamesInflection::getCase($model_data_profile_list->model_geo_object->name, 'prepositional').
                ' онлайн бесплатно, найдите пару или партнера в '.
                morphos\Russian\GeographicalNamesInflection::getCase(
                    $model_data_profile_list->model_geo_object->name
                    , 
                    'prepositional'
                ).
                ' '.
                morphos\Russian\GeographicalNamesInflection::getCase(
                    $parent_geo->name
                    , 
                    'prepositional'
                ).
                ' для свинга, свингеры '.
                morphos\Russian\GeographicalNamesInflection::getCase($model_data_profile_list->model_geo_object->name, 'genitive').
                ' знакомятся на '.APP_NAME.'.',
                true
            );
        }
        
        
    }



    // Устанавливаем Title страницы
    \xtetis\xengine\helpers\SeoHelper::setTitle($h1.' c фотографиями онлайн - ' . APP_NAME);



    // Добавляем файл JS для обработки страницы
    \xtetis\xengine\helpers\StatResHelper::addTemplateJs(__DIR__ . '/../../web/js/xdate.js');


?>


<h1>
    <?=$h1?>
</h1>

<?=\xtetis\xdate\Component::renderBlock(
    'blocks/profile_search_form',
    [
        'model_data_profile_list' => $model_data_profile_list,
        'urls'                    => $urls,
    ]
)?>


<div class="card-columns">

    <?php foreach ($model_data_profile_list->model_date_profile_list as $id_profile => $date_profile_model): ?>
        <?=\xtetis\xdate\Component::renderBlock(
            'blocks/profile_list_item',
            [
                'date_profile_model'=>$date_profile_model,
            ]
        ); 
        ?>
    <?php endforeach;?>

</div>
<div class="pagination_container">
    <?=$paginate?>
</div>
<?php if (strlen($h2)):?>
<h2 style="    font-weight: unset;
    color: gray;
    font-size: 0.9em;">
    <?=$h2?>
</h2>
<?php endif;?>