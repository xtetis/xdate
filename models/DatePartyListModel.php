<?php

namespace xtetis\xdate\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class DatePartyListModel extends \xtetis\xengine\models\Model
{

    /**
     * Смещение
     */
    public $offset = 0;

    /**
     * Лимит
     */
    public $limit = 18;

    /**
     * @var int
     */
    public $total_count = 0;

    /**
     * Список найденных моделей
     */
    public $model_list = [];

    /**
     * @var array
     */
    public $where_arr = [];

    /**
     * Показывать завершенные
     */
    public $show_finished = 0;

    /**
     * Показывать неопубликованные
     */
    public $show_unpublished = 0;

    /**
     * Возвращает  общее количество по параметрам
     */
    public function getTotalCount()
    {
        if ($this->getErrors())
        {
            return false;
        }

        if ($this->total_count)
        {
            return $this->total_count;
        }

        $sql = $this->getListSql(true);

        $this->total_count = \xtetis\xdate\models\DatePartyModel::getCountBySql($sql);

        return true;
    }

    /**
     * Возвращает SQL для получения данных о профилях знакомств 
     * (как для количества, так и для списка моделей)
     */
    public function getListSql($count = true)
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->offset          = intval($this->offset);
        $this->limit          = intval($this->limit);
        $this->show_finished = intval($this->show_finished);


        $select_fields = 'p.id';
        if ($count)
        {
            $select_fields = 'COUNT(*) as count';
        }

        $sql = 'SELECT ' . $select_fields . ' FROM xdate_party p WHERE  ';

        $this->where_arr = [];

        $this->where_arr[] = 'TRUE';

        if (!$this->show_finished)
        {
            $this->where_arr[] = 'p.party_date > CURRENT_DATE';
        }

        
        if (!$this->show_unpublished)
        {
            $this->where_arr[] = 'p.published = 1';
        }

        


        $sql .= implode(' AND ', $this->where_arr);

        if (!$count)
        {
            $sql .= ' ORDER BY p.party_date DESC ' ;

            if ($this->offset)
            {
                $sql .= ' OFFSET ' . $this->offset;
            }

            if ($this->limit)
            {
                $sql .= ' LIMIT ' . $this->limit;
            }
        }

        return $sql;
    }

    /**
     *
     */
    public function validateParams()
    {
        if ($this->getErrors())
        {
            return false;
        }


        return true;

    }

    /**
     * Возвращает список моделей
     */
    public function getModelList()
    {
        if ($this->getErrors())
        {
            return false;
        }

        if (!$this->validateParams())
        {
            return false;
        }

        $this->getTotalCount();

        $sql = $this->getListSql(false);

        $this->model_list = \xtetis\xdate\models\DatePartyModel::getModelListBySql($sql);

        return $this->model_list;

    }



}
