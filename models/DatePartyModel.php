<?php

namespace xtetis\xdate\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class DatePartyModel extends \xtetis\xengine\models\TableModel
{

    /**
     * ID статьи
     */
    public $id = 0;

    /**
     * @var string
     */
    public $table_name = 'xdate_party';

    /**
     * Возвращает связанную модель или false
     * Модели с аттрибутами связываются в массиве related_rule_list, например
     *
     * $this->related_rule_list = [
     *      'attribute_name' => \xtetis\xanyfield\models\ComponentModel::class
     * ];
     *
     * Связанная модель получается методом $this->getModelRelated('attribute_name')
     */
    public $related_rule_list = [
        'id_user'          => \xtetis\xuser\models\UserModel::class,
        'id_gallery_cover' => \xtetis\ximg\models\GalleryModel::class,
        'id_gallery_place' => \xtetis\ximg\models\GalleryModel::class,
    ];

    /**
     * Назвавние вечеринки
     */
    public $name = '';

    /**
     * Текст вечеринки
     */
    public $about = '';

    /**
     * ID пользователя автора
     */
    public $id_user = 0;

    /**
     * ID города
     */
    public $id_city = 0;

    /**
     * ID региона
     */
    public $id_region = 0;

    /**
     * ID страны
     */
    public $id_country = 0;

    /**
     * Дата проведения вечеринки
     */
    public $party_date = '';

    /**
     * Массив с каоритками в base64 для обложки вечеринки
     */
    public $image_cover = [];

    /**
     * Массив с каоритками в base64 для места проведения вечеринки
     */
    public $image_party_place = [];

    /**
     * ID галереи для обложек вечеринки
     */
    public $id_gallery_cover = 0;

    /**
     * ID галереи для места проведения вечеринки
     */
    public $id_gallery_place = 0;

    /**
     * Возвращает текст с описанием метоположения
     * СТрана -> Регион -> Город
     */
    public function getPartyLocationText()
    {
        return '';
    }

    /**
     * Сохраняет вечеринку в базе
     */
    public function addParty()
    {
        if ($this->getErrors())
        {
            return false;
        }

        // Проверяет параметры
        \xtetis\xdate\Config::validateParams();

        $this->name       = strval($this->name);
        $this->about      = strval($this->about);
        $this->party_date = strval($this->party_date);
        $this->id_city    = intval($this->id_city);

        // Список полей, которые участвуют в INSERT
        $this->insert_update_field_list = [
            'name',
            'about',
            'party_date',
            'id_user',
            'id_city',
            'id_region',
            'id_country',
        ];

        if (!\xtetis\xuser\Component::isLoggedIn())
        {
            $this->addError('id_user', 'Только для авторизированных пользователей');

            return false;
        }

        if (!is_array($this->image_cover))
        {
            $this->addError('image_cover', 'Выберите изображение в качестве обложки вечеринки');

            return false;
        }

        if (!count($this->image_cover))
        {
            $this->addError('image_cover', 'Выберите изображение в качестве обложки вечеринки');

            return false;
        }

        if (count($this->image_cover) != 1)
        {
            $this->addError('image_cover', 'Изображение для обложки вечеринки должно быть только одно');

            return false;
        }

        // Проверяем каждую картинку на возможность загрузки
        foreach ($this->image_cover as $image_cover_b64)
        {
            $model_upload_image = new \xtetis\ximg\models\ImgUploadModel([
                'filedata' => $image_cover_b64,
            ]);

            // Проверяем параметры файла и возможность загрузки файла
            if (!$model_upload_image->checkUploadFile())
            {
                $this->addError('image_cover', $model_upload_image->getLastErrorMessage());

                return false;
            }
        }

        if (!is_array($this->image_party_place))
        {
            $this->addError('image_party_place', 'Выберите изображение в качестве места проведения вечеринки');

            return false;
        }

        if (count($this->image_party_place) > 5)
        {
            $this->addError('image_cover', 'Изображений для места проведения вечеринки должно быть не более 5');

            return false;
        }

        // Проверяем каждую картинку на возможность загрузки
        foreach ($this->image_party_place as $image_party_place_b64)
        {
            $model_upload_image = new \xtetis\ximg\models\ImgUploadModel([
                'filedata' => $image_party_place_b64,
            ]);

            // Проверяем параметры файла и возможность загрузки файла
            if (!$model_upload_image->checkUploadFile())
            {
                $this->addError('image_party_place_b64', $model_upload_image->getLastErrorMessage());

                return false;
            }
        }

        $this->id_user = \xtetis\xuser\Component::isLoggedIn()->id;

        if (!strlen($this->name))
        {
            $this->addError('name', 'Не заполнено название вечринки');

            return false;
        }

        if (mb_strlen($this->name) < 5)
        {
            $this->addError('name', 'Минимальная длина название вечеринки 5 символов');

            return false;
        }

        if (mb_strlen($this->name) > 100)
        {
            $this->addError('name', 'Максимальная длина название вечеринки 100 символов');

            return false;
        }

        if (strip_tags($this->name) != $this->name)
        {
            $this->addError('name', 'Не допустимы HTML теги в названии вечеринки');

            return false;
        }

        if (!strlen($this->about))
        {
            $this->addError('about', 'Не заполнено описание вечринки');

            return false;
        }

        if (mb_strlen($this->about) < 100)
        {
            $this->addError('about', 'Минимальная длина описания вечеринки 100 символов');

            return false;
        }

        if (preg_replace('/[^0-9\.]/', '', $this->party_date) != $this->party_date)
        {
            $this->addError('party_date', 'Тип date подразумевает наличие цифры и точки');

            return false;
        }

        if (mb_strlen($this->party_date) != 10)
        {
            $this->addError('party_date', 'Длина поля типа date должна равняться 10 символам');

            return false;
        }

        if (!preg_match("/^\d{2}\.\d{2}\.\d{4}$/", $this->party_date))
        {
            $this->addError('party_date', 'Некорректный формат даты');

            return false;
        }

        list($d, $m, $y) = array_pad(explode('.', $this->party_date, 3), 3, 0);

        if (!ctype_digit("$d$m$y"))
        {
            $this->addError('party_date', 'Некорректная дата');

            return false;
        }

        if (!checkdate($m, $d, $y))
        {
            $this->addError('party_date', 'Некорректная дата');

            return false;
        }

        if (strtotime($this->party_date) < strtotime('today'))
        {
            $this->addError('party_date', 'Дата вечеринки должна быть позднее сегодняшнего дня');

            return false;
        }

        if (!$this->id_city)
        {
            $this->addError('id_city', 'Не указан город');

            return false;
        }

        $model_city = \xtetis\xgeo\models\CityModel::generateModelById($this->id_city);
        if (!$model_city)
        {
            $this->addError('id_city', 'Город не существует');

            return false;
        }

        $model_region = $model_city->getModelRelated('id_region');
        if (!$model_region)
        {
            $this->addError('id_city', 'Регион не существует');

            return false;
        }

        $model_country = $model_region->getModelRelated('id_country');
        if (!$model_country)
        {
            $this->addError('id_city', 'Страна не существует');

            return false;
        }

        $this->id_city    = $model_city->id;
        $this->id_region  = $model_region->id;
        $this->id_country = $model_country->id;

        if (!$this->insertTableRecord())
        {
            $this->addError('common', $this->getLastErrorMessage());

            return false;
        }

        // Создаем галерею для обложки
        // ---------------------------------------------
        $model_gallery_cover = new \xtetis\ximg\models\GalleryModel([
            'id_category' => ID_PARTY_GALLERY_PARENT_CATEGORY,
            'name'        => 'party_image_cover_' . $this->id,
        ]);

        if (!$model_gallery_cover->addGallery())
        {
            $this->addError('image_cover', $model_gallery_cover->getLastErrorMessage());

            return false;
        }

        // Загружаем изображения обложки
        foreach ($this->image_cover as $image_cover_b64)
        {
            $img_model = new \xtetis\ximg\models\ImgModel([
                'id_gallery' => $model_gallery_cover->id,
                'filedata'   => $image_cover_b64,
            ]);
            if (!$img_model->uploadImageAndSaveInGallery())
            {
                $this->addError('image_cover', $img_model->getLastErrorMessage());

                return false;
            }
        }
        // ---------------------------------------------

        // Создаем галерею для места проведения вечеринки
        // ---------------------------------------------
        $model_gallery_place = new \xtetis\ximg\models\GalleryModel([
            'id_category' => ID_PARTY_GALLERY_PARENT_CATEGORY,
            'name'        => 'party_image_place_' . $this->id,
        ]);

        if (!$model_gallery_place->addGallery())
        {
            $this->addError('image_cover', $model_gallery_place->getLastErrorMessage());

            return false;
        }

        // Загружаем изображения обложки
        foreach ($this->image_party_place as $image_party_place_b64)
        {
            $img_model = new \xtetis\ximg\models\ImgModel([
                'id_gallery' => $model_gallery_place->id,
                'filedata'   => $image_party_place_b64,
            ]);
            if (!$img_model->uploadImageAndSaveInGallery())
            {
                $this->addError('image_party_place', $img_model->getLastErrorMessage());

                return false;
            }
        }
        // ---------------------------------------------

        // Если все успешно залилось - обновляем в БД id_gallery_cover
        $this->id_gallery_cover = $model_gallery_cover->id;

        // Если все успешно залилось - обновляем в БД id_gallery_cover
        $this->id_gallery_place = $model_gallery_place->id;

        // Список полей, которые обновить
        $this->insert_update_field_list = ['id_gallery_cover', 'id_gallery_place'];

        // Обновляем данные в таблице
        $this->updateTableRecordById();

        return true;
    }

    /**
     * Возвращает модель пользователя по текущему профилю знакомств
     */
    public function getModelUser()
    {
        if ($this->getErrors())
        {
            return false;
        }

        return $this->getModelRelated('id_user');
    }

    /**
     * Возвращает текст с выбранной локацией
     */
    public function getLocationText()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_city = intval($this->id_city);

        $model_city = \xtetis\xgeo\models\CityModel::generateModelById($this->id_city);
        if (!$model_city)
        {
            return '';
        }

        $model_region = $model_city->getModelRelated('id_region');
        if (!$model_region)
        {
            return '';
        }

        $model_country = $model_region->getModelRelated('id_country');
        if (!$model_country)
        {
            return '';
        }

        return $model_country->name . ' -> ' . $model_region->name . ' -> ' . $model_city->name;

    }

    /**
     * Возвращает ссылку на вечеринку
     */
    public function getLinkToParty()
    {
        if ($this->getErrors())
        {
            return false;
        }

        return \xtetis\xdate\Component::makeUrl(['path' => [
            'party',
            'item',
            $this->id,
        ]]);
    }

    /**
     * Возвращает главное изображение вечеринки
     */
    public function getMainImgSrc()
    {
        if ($this->getErrors())
        {
            return false;
        }
        // Главное изображение
        $src                 = \xtetis\ximg\models\ImgModel::getNoimageSrc();
        $model_gallery_cover = $this->getModelRelated('id_gallery_cover');
        if ($model_gallery_cover)
        {
            // SRC главной картинки
            $src = $model_gallery_cover->getMainImgSrc();
        }

        return $src;
    }

}
