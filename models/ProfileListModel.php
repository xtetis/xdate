<?php

namespace xtetis\xdate\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class ProfileListModel extends \xtetis\xengine\models\Model
{

    /**
     * Смещение
     */
    public $offset = 0;

    /**
     * Лимит
     */
    public $limit = 20;

    /**
     * Тип профиля знакомств
     */
    public $id_profile_type = 0;

    /**
     * @var int
     */
    static $min_age = 18;

    /**
     * Максимальный возраст
     */
    static $max_age = 99;
    
    /**
     * Допустимые уровни  локации
     * 1 - страна
     * 2 - регион
     * 3 - город
     */
    static $allow_location_level = [1,2,3];

    /**
     * Возраст от
     */
    public $age_start = 0;

    /**
     * Возраст до
     */
    public $age_end = 0;



    /**
     * Список моделей DateProfile
     */
    public $model_date_profile_list = [];

    /**
     * @var int
     */
    public $total_count = 0;

    /**
     * @var array
     */
    public $where_arr = [];

    /**
     * ID выбранной локации
     */
    public $id_location = 0;


    /**
     * Модель ГЕО объекта
     */
    public $model_geo_object = false;

    /**
     * Условия сортировки
     */
    public $order = ' `id` DESC ';

    /**
     * Результат получения данных из SQL запроса
     *
     * getById - Возвращает статью по ID
     * saveArticle - Сохраняем статью
     */
    public $result_sql = [];

    /**
     * Возвращает  общее количество профилей по параметрам
     */
    public function getTotalCount()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $sql = $this->getDateProfileSql(true);

        $this->result_sql['dynamicSql'] = \xtetis\xdate\models\SqlModel::dynamicSql(
            $sql,
            true
        );

        if (!$this->result_sql['dynamicSql'])
        {
            $this->total_count = 0;

            return true;
        }

        $row               = $this->result_sql['dynamicSql'];
        $this->total_count = intval($row['count']);

        return true;
    }

    /**
     * Возвращает SQL для получения данных о профилях знакомств 
     * (как для количества, так и для списка моделей)
     */
    public function getDateProfileSql($count = true)
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->offset          = intval($this->offset);
        $this->limit           = intval($this->limit);
        $this->id_profile_type = intval($this->id_profile_type);
        $this->age_end         = intval($this->age_end);
        $this->id_location       = intval($this->id_location);

        $select_fields = 'p.id';
        if ($count)
        {
            $select_fields = 'COUNT(*) as count';
        }

        $sql = 'SELECT ' . $select_fields . ' FROM xdate_profile p WHERE  ';


        $this->where_arr[] = 'TRUE';

        $this->where_arr[] = ' COALESCE(p.id_profile_type,0) > 0 ';

        if ($this->id_profile_type)
        {
            $this->where_arr[] = ' p.id_profile_type = ' . $this->id_profile_type;

        }

        if ($this->age_start)
        {
            $this->where_arr[] = ' TIMESTAMPDIFF(YEAR, COALESCE(p.birth_date,current_date), current_date) >= ' . $this->age_start;
        }

        if ($this->age_end)
        {
            $this->where_arr[] = ' TIMESTAMPDIFF(YEAR, COALESCE(p.birth_date,current_date), current_date) <= ' . $this->age_end;
        }

        if ($this->id_location)
        {
            $this->where_arr[] = ' p.id_geo_object IN (

                SELECT id FROM `xgeo_object` WHERE `id` = '.$this->id_location.'
                UNION 
                SELECT id FROM `xgeo_object` WHERE `id_parent` = '.$this->id_location.'
                UNION
                SELECT id FROM `xgeo_object` WHERE `id_parent` IN 
                (
                    SELECT id FROM `xgeo_object` WHERE `id_parent` = '.$this->id_location.'
                )
                
            )';
        }


        $sql .= implode(' AND ', $this->where_arr);

        if (isset($this->order))
        {
            $order = ' ORDER BY '.$this->order.' ';
            $sql .= $order;
        }

        if (!$count)
        {
            if ($this->limit)
            {
                $sql .= ' LIMIT ' . $this->limit;
            }

            if ($this->offset)
            {
                $sql .= ' OFFSET ' . $this->offset;
            }

        }

        return $sql;
    }

    /**
     *
     */
    public function validateParams()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_profile_type   = intval($this->id_profile_type);
        $this->age_start         = intval($this->age_start);
        $this->age_end           = intval($this->age_end);
        $this->id_location       = intval($this->id_location);



        // Проверяем тип профиля
        if ($this->id_profile_type)
        {
            $model_dateprofile_type = \xtetis\xdate\models\ProfileTypeModel::generateModelById($this->id_profile_type);
            if (!$model_dateprofile_type)
            {
                $this->addError('id_profile_type', 'Тип профиля некорректный');

                return false;
            }
        }

        // Проверяем минимальный возраст
        if ($this->age_start)
        {
            if ($this->age_start < self::$min_age)
            {
                $this->addError('age_start', 'Минимальный возраст ' . self::$min_age . ' лет');

                return false;
            }

            if ($this->age_start > self::$max_age)
            {
                $this->addError('age_start', 'Максимальный возраст ' . self::$max_age . ' лет');

                return false;
            }
        }

        // Проверяем максимальный возраст
        if ($this->age_end)
        {
            if ($this->age_end < self::$min_age)
            {
                $this->addError('age_end', 'Минимальный возраст ' . self::$min_age . ' лет');

                return false;
            }

            if ($this->age_end > self::$max_age)
            {
                $this->addError('age_end', 'Максимальный возраст ' . self::$max_age . ' лет');

                return false;
            }
        }

        // Проверяем минимальный и максимальный возраст
        if (($this->age_end) && ($this->age_start))
        {
            if ($this->age_end < $this->age_start)
            {
                $this->addError('age_end', 'Минимальный возраст не может быть больше максимального');

                return false;
            }
        }


        
        // Проверяем локацию
        if ($this->id_location)
        {

            $this->model_geo_object = \xtetis\xgeo\models\ObjectModel::generateModelById($this->id_location);
            if (!$this->model_geo_object)
            {
                $this->addError('id_location', 'Локация не существует');

                return false;
            }
        }

        return true;

    }

    /**
     * Возвращает список моделей профилей знакомств
     */
    public function getProfileModelList()
    {
        if ($this->getErrors())
        {
            return false;
        }

        if (!$this->validateParams())
        {
            return false;
        }

        // Количество профилей согласно параметрам
        $this->getTotalCount();

        $sql = $this->getDateProfileSql(false);

        $this->result_sql['dynamicSql'] = \xtetis\xdate\models\SqlModel::dynamicSql(
            $sql,
            false
        );

        if (!$this->result_sql['dynamicSql'])
        {
            $this->model_date_profile_list = [];

            return $this->model_date_profile_list;
        }

        foreach ($this->result_sql['dynamicSql'] as $row)
        {
            $id = intval($row['id']);
            $date_profile_model = \xtetis\xdate\models\ProfileModel::generateModelById($id);
            if (!$date_profile_model)
            {
                $this->addError('date_profile_model' , 'Не найден профиль знакомств #'.$id);

                return false;
            }

            $this->model_date_profile_list[$id] = $date_profile_model;
        }

        return $this->model_date_profile_list;

    }

    /**
     * Возвращает опции для возраста
     *
     * @return mixed
     */
    public function getAgeOptions(
        $add_root = true
    )
    {
        $age_options = [];

        if ($add_root)
        {
            $age_options[0] = 'Не указано';
        }

        for ($i = self::$min_age; $i <= self::$max_age; $i++)
        {
            $age_options[$i] = $i;
        }

        return $age_options;
    }

    /**
     * Возвращает текст с выбранной локацией или 
     * пустую строку в случае неудачи
     */
    public function getSearchLocationText()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_location       = intval($this->id_location);

        // Проверяем локацию
        if ($this->id_location)
        {
            $this->model_geo_object = \xtetis\xgeo\models\ObjectModel::generateModelById($this->id_location);
            if ($this->model_geo_object)
            {
                return $this->model_geo_object->getFullTreePath();
            }
        }

        return '';
    }


    /**
     * Возвращает ссылку на поиск
     */
    public function getLinkSearch()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $ret = \xtetis\xdate\Component::makeUrl();

        $this->id_location       = intval($this->id_location);

        if (!$this->id_location)
        {
            return $ret;
        }

        $model_geo_object = \xtetis\xgeo\models\ObjectModel::generateModelById(
            $this->id_location
        );

        if (!$model_geo_object)
        {
            return $ret;
        }

        $ret = \xtetis\xdate\Component::makeUrl([
            'query'=>[
                'id_location'=>$model_geo_object->id
            ]
        ]);

        if (strlen(strval($model_geo_object->sef)))
        {
            $ret = \xtetis\xdate\Component::makeUrl([
                'path'=>[
                    'geo_'.$model_geo_object->sef
                ]
            ]);
        }

        return $ret;
    }

}
