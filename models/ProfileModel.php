<?php

namespace xtetis\xdate\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class ProfileModel extends \xtetis\xengine\models\TableModel
{

    /**
     * ID статьи
     */
    public $id = 0;

    /**
     * Таблица по которой работает модель
     */
    public $table_name = 'xdate_profile';

    /**
     * Возвращает связанную модель или false
     * Модели с аттрибутами связываются в массиве related_rule_list, например
     *
     * $this->related_rule_list = [
     *      'attribute_name' => \xtetis\xanyfield\models\ComponentModel::class
     * ];
     *
     * Связанная модель получается методом $this->getModelRelated('attribute_name')
     */
    public $related_rule_list = [
        'id_user'         => \xtetis\xuser\models\UserModel::class,
        'id_profile_type' => \xtetis\xdate\models\ProfileTypeModel::class,
        'id_geo_object'   => \xtetis\xgeo\models\ObjectModel::class,
    ];

    /**
     * ID пользователя
     */
    public $id_user = 0;

    /**
     * @var int
     */
    public $id_city = 0;

    /**
     * ID региона
     */
    public $id_region = 0;

    /**
     * ШВ страны
     */
    public $id_country = 0;

    /**
     * Привязанный ГЕО объект - где находится пользователь
     */
    public $id_geo_object = 0;

    /**
     * Тип профиля
     */
    public $id_profile_type = 0;

    /**
     * Дата рождения
     */
    public $birth_date = '';

    /**
     * Дата создания профиля знакомств
     */
    public $create_date = '';

    /**
     * Рост (см)
     */
    public $height = 0;

    /**
     * Вес (кг)
     */
    public $weight = 0;

    /**
     * Ориентация
     */
    public $orientation = 0;

    /**
     * Семейное положение
     */
    public $marital_status = 0;

    /**
     * Вы изменяете?
     */
    public $cheating_status = 0;

    /**
     * О себе
     */
    public $about = 0;

    /**
     * Модель пользователя, который привязан к текущему профилю знакомств
     */
    public $model_user = false;

    /**
     * Модель страны, который привязан к текущему профилю знакомств
     */
    public $model_country = false;

    /**
     * Модель региона, который привязан к текущему профилю знакомств
     */
    public $model_region = false;

    /**
     * Модель города, который привязан к текущему профилю знакомств
     */
    public $model_city = false;

    /**
     * Результат получения данных из SQL запроса
     *
     * getById - Возвращает статью по ID
     * saveArticle - Сохраняем статью
     */
    public $result_sql = [];

    /**
     * Возвращает информацию о статье по ID
     */
    public function getById(
    )
    {
        if ($this->getErrors())
        {
            return false;
        }

        if (!$this->getModelById())
        {
            return false;
        }

        return true;
    }

    /**
     * Возвращает профиль пользователя по ID пользователя
     */
    public function getByUserId(
    )
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_user = intval($this->id_user);

        if (!$this->id_user)
        {
            $this->addError('id_user', __FUNCTION__ . ': Не указан id_user');

            return false;
        }

        // Возвращает профиль пользователя (знакомства) по ID пользователя
        $this->result_sql['getDateProfileByUserId'] = \xtetis\xdate\models\SqlModel::getDateProfileByUserId(
            $this->id_user
        );

        if (!$this->result_sql['getDateProfileByUserId'])
        {

            // Добавляет DateProfile для указанного пользователя
            $this->result_sql['addDateProfile'] = \xtetis\xdate\models\SqlModel::addDateProfile(
                $this->id_user
            );

        }

        // Возвращает профиль пользователя (знакомства) по ID пользователя
        $this->result_sql['getDateProfileByUserId'] = \xtetis\xdate\models\SqlModel::getDateProfileByUserId(
            $this->id_user
        );

        if (!$this->result_sql['getDateProfileByUserId'])
        {
            $this->addError('id_user', __FUNCTION__ . ': Не найдена запись в xdate_profile по id_user');

            return false;
        }

        $this->id = $this->result_sql['getDateProfileByUserId']['id'];

        if (!$this->getModelById())
        {
            return false;
        }

        return true;
    }

    /**
     * Сохраняет профиль пользователя
     */
    public function saveDateProfile(
    )
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_user         = intval($this->id_user);
        $this->birth_date      = strval($this->birth_date);
        $this->id_profile_type = intval($this->id_profile_type);
        $this->id_geo_object = intval($this->id_geo_object);
        $this->about         = strval($this->about);
        $this->about         = strip_tags($this->about);
        $this->height        = intval($this->height);
        $this->weight        = intval($this->weight);

        $this->model_user = \xtetis\xuser\Component::isLoggedIn();
        if (!$this->model_user)
        {
            $this->addError('id_user', 'Пользователь не авторизирован');

            return false;
        }

        if (!$this->id_user)
        {
            $this->id_user = $this->model_user->id;
        }


        if (!$this->id_user)
        {
            $this->addError('id_user', 'Не указан id_user');

            return false;
        }

        if (!$this->id_profile_type)
        {
            $this->addError('id_profile_type', 'Не указан тип профиля');

            return false;
        }

        if (!mb_strlen($this->about))
        {
            $this->addError('about', 'Напишите о себе несколько слов');

            return false;
        }

        if (!isset($this->getHeightOptions(true)[$this->height]))
        {
            $this->addError('height', 'Укажите корректный возраст');

            return false;
        }

        if (!isset($this->getWeightOptions(true)[$this->weight]))
        {
            $this->addError('weight', 'Укажите корректный вес');

            return false;
        }

        if (!strlen($this->birth_date))
        {
            $this->addError('birth_date', 'Не указана дата рождения');

            return false;
        }

        if (!preg_match("/^\d{2}\.\d{2}\.\d{4}$/", $this->birth_date))
        {
            $this->addError('birth_date', 'Некорректный формат даты рождения');

            return false;
        }

        list($d, $m, $y) = array_pad(explode('.', $this->birth_date, 3), 3, 0);

        if (!ctype_digit("$d$m$y"))
        {
            $this->addError('birth_date', 'Некорректная дата рождения');

            return false;
        }

        if (!checkdate($m, $d, $y))
        {
            $this->addError('birth_date', 'Некорректная дата рождения');

            return false;
        }

        $birth_date = strtotime($this->birth_date);

        if ($birth_date > strtotime('-18 years', time()))
        {
            $this->addError('birth_date', 'Только для тех, кто достиг 18 лет');

            return false;
        }

        if ($birth_date < strtotime('-99 years', time()))
        {
            $this->addError('birth_date', 'Вам слишком много лет');

            return false;
        }

        $date_profile_type_model   = new \xtetis\xdate\models\ProfileTypeModel();
        $date_profile_type_options = $date_profile_type_model->getOptions();

        if (!isset($date_profile_type_options[$this->id_profile_type]))
        {
            $this->addError('id_profile_type', 'Не указан тип профиля');

            return false;
        }

        if (!$this->id_geo_object)
        {
            $this->addError('id_geo_object', 'Не указан населенный пункт');

            return false;
        }

        $model_geo_object = \xtetis\xgeo\models\ObjectModel::generateModelById($this->id_geo_object);
        if (!$model_geo_object)
        {
            $this->addError('id_geo_object', 'Населенный пункт не существует');

            return false;
        }


        $model_date_profile = new self([
            'id_user' => $this->id_user,
        ]);
        $model_date_profile->getByUserId();
        if ($model_date_profile->getErrors())
        {
            $this->addError('common', $model_date_profile->getLastErrorMessage());

            return false;
        }

        $this->id = $model_date_profile->id;

        $this->insert_update_field_list = [
            'id_profile_type',
            'birth_date',
            'id_geo_object',
            'about',
            'height',
            'weight',
        ];
        if (!$this->updateTableRecordById())
        {
            return false;
        }

        return true;
    }

    /**
     * Возвращает возраст
     */
    public function getProfileAgeYearsText()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->birth_date = strval($this->birth_date);

        $bday  = new \DateTime($this->birth_date); // Your date of birth
        $today = new \Datetime(date('m.d.y'));
        $diff  = $today->diff($bday);

        return $diff->y;
    }

    /**
     * Возвращает суффикс для количества лет профиля (год, года, лет)
     */
    public function getAgeYearsSuffix()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $year = $this->getProfileAgeYearsText();
        $year = abs(intval($year));
        $t1   = $year % 10;
        $t2   = $year % 100;

        return (1 == $t1 && 11 != $t2 ? 'год' : ($t1 >= 2 && $t1 <= 4 && ($t2 < 10 || $t2 >= 20) ? 'года' : 'лет'));
    }

    /**
     * Возвращает модель пользователя по текущему профилю знакомств
     */
    public function getModelUser()
    {
        if ($this->getErrors())
        {
            return false;
        }

        return $this->getModelRelated('id_user');
    }

    /**
     * Возвращает модель страны по текущему профилю знакомств
     */
    public function getModelCountry()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_country = intval($this->id_country);
        if (!$this->id_country)
        {
            $this->addError('id_country', 'Не указан id_country для текущего профиля знакомств');

            return false;
        }

        if ($this->model_country)
        {
            return $this->model_country;
        }

        $this->model_country = new \xtetis\xgeo\models\CountryModel([
            'id' => $this->id_country,
        ]);

        $this->model_country->getModelById();

        if ($this->model_country->getErrors())
        {
            $this->addError('model_country', $this->model_country->getLastErrorMessage());

            return false;
        }

        return $this->model_country;
    }

    /**
     * Возвращает модель региона по текущему профилю знакомств
     */
    public function getModelRegion()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_region = intval($this->id_region);
        if (!$this->id_region)
        {
            $this->addError('id_region', 'Не указан id_region для текущего профиля знакомств');

            return false;
        }

        if ($this->model_region)
        {
            return $this->model_region;
        }

        $this->model_region = new \xtetis\xgeo\models\RegionModel([
            'id' => $this->id_region,
        ]);

        $this->model_region->getModelById();

        if ($this->model_region->getErrors())
        {
            $this->addError('model_region', $this->model_region->getLastErrorMessage());

            return false;
        }

        return $this->model_region;
    }

    /**
     * Возвращает модель города по текущему профилю знакомств
     */
    public function getModelCity()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_city = intval($this->id_city);
        if (!$this->id_city)
        {
            $this->addError('id_city', 'Не указан id_city для текущего профиля знакомств');

            return false;
        }

        if ($this->model_city)
        {
            return $this->model_city;
        }

        $this->model_city = new \xtetis\xgeo\models\CityModel([
            'id' => $this->id_city,
        ]);

        $this->model_city->getModelById();

        if ($this->model_city->getErrors())
        {
            $this->addError('model_city', $this->model_city->getLastErrorMessage());

            return false;
        }

        return $this->model_city;
    }

    /**
     * Возвращает модель ГЕО объекта
     */
    public function getModelGeoObject()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_geo_object = intval($this->id_geo_object);
        if (!$this->id_geo_object)
        {
            return false;
        }

        return $this->getModelRelated('id_geo_object');
    }

    /**
     * Возвращает текст с выбранной локацией (полный путь)
     */
    public function getProfileLocationFullText()
    {
        if ($this->getErrors())
        {
            return '';
        }

        $model_geo_object = $this->getModelGeoObject();
        if (!$model_geo_object)
        {
            return '';
        }

        return $model_geo_object->getFullTreePath();
    }

    /**
     * Возвращает текст с выбранной локацией - имя с учетом 
     * родительских элементов (N-го уровня вложенности)
     */
    public function getProfileLocationPartText($deep_level = 2)
    {
        if ($this->getErrors())
        {
            return '';
        }

        $model_geo_object = $this->getModelGeoObject();
        if (!$model_geo_object)
        {
            return '';
        }

        return $model_geo_object->getPartTreePath($deep_level);
    }

    /**
     * Возвращает текст с выбранной локацией (только конечный пункт в дереве локаций)
     * Если нужно преобразует по падежам
     * - родительный
     */
    public function getProfileLocationText($padezh = '')
    {
        if ($this->getErrors())
        {
            return '';
        }

        $model_geo_object = $this->getModelGeoObject();
        if (!$model_geo_object)
        {
            return '';
        }

        if (!strlen($padezh))
        {
            return $model_geo_object->name;
        }
        else
        {
            return     \morphos\Russian\GeographicalNamesInflection::getCase(
                $model_geo_object->name,
                $padezh
            );
        }
        
    }





    /**
     * Возвращает текст с выбранным городом
     */
    public function getProfileCityText()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_city = intval($this->id_city);

        $model_city = \xtetis\xgeo\models\CityModel::generateModelById($this->id_city);
        if (!$model_city)
        {
            return '';
        }

        return $model_city->name;

    }

    /**
     * Возвращает ссылку на профиль
     */
    public function getLink($params = [])
    {
        if ($this->getErrors())
        {
            return false;
        }

        $with_host = false;
        if (isset($params['with_host']))
        {
            $with_host = $params['with_host'];
        }

        return \xtetis\xdate\Component::makeUrl([
            'path'      => [
                'profile',
                $this->id,
            ],
            'with_host' => $with_host,
        ]);
    }

    /**
     * Возвращает текст с типом анкеты или пустую строку
     */
    public function getTypeProfileText()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_profile_type = intval($this->id_profile_type);
        $model_profile_type    = \xtetis\xdate\models\ProfileTypeModel::generateModelById($this->id_profile_type);
        if (!$model_profile_type)
        {
            return '';
        }

        return $model_profile_type->name;
    }

    /**
     * Возвращает главное изображение профиля
     */
    public function getProfileMainImgSrc()
    {
        if ($this->getErrors())
        {
            return false;
        }

        // Главное изображение
        $model_date_album_list = \xtetis\xdate\models\DateAlbumModel::getDateAlbumModelListByUserId($this->id_user);

        // Если нет главного изображения
        $src = \xtetis\ximg\models\ImgModel::getNoimageSrc();

        // Получаем список главных изображений
        foreach ($model_date_album_list as $id_date_album => $model_date_album)
        {
            if ($model_date_album->is_main)
            {
                $model_gallery = $model_date_album->getModelGallery();
                if ($model_gallery)
                {
                    $src = $model_gallery->getMainImgSrc();
                }
            }
        }

        return $src;
    }

    /**
     * Возвращает опции роста в СМ
     */
    public static function getHeightOptions($add_empty = false)
    {
        $ret = [];

        if ($add_empty)
        {
            $ret[0] = 'Не указано';
        }

        for ($i = 100; $i < 200; $i++)
        {
            $ret[$i] = $i;
        }

        return $ret;
    }

    /**
     * Возвращает опции весв (кг)
     */
    public static function getWeightOptions($add_empty = false)
    {
        $ret = [];
        if ($add_empty)
        {
            $ret[0] = 'Не указано';
        }

        for ($i = 1; $i < 200; $i++)
        {
            $ret[$i] = $i;
        }

        return $ret;
    }

    /**
     * Возвращает опции ориентации
     */
    public static function getOrientationOptions($add_empty = false)
    {
        $ret = [];
        if ($add_empty)
        {
            $ret[0] = 'Не указано';
        }

        $ret[1] = 'Гетеро';
        $ret[2] = 'Би';
        $ret[3] = 'C\п: он гетеро, она гетеро';
        $ret[4] = 'с\п: он гетеро, она би';
        $ret[5] = 'с\п: он би, она гетеро';
        $ret[6] = 'с\п: он би, она би';
        $ret[7] = 'Свой вариант';

        return $ret;
    }

    /**
     * Возвращает рост как текст
     */
    public function getHeightText()
    {
        $ret = '';
        if (isset($this->getHeightOptions(true)[$this->height]))
        {
            $ret = $this->getHeightOptions(true)[$this->height] . ' см';
            if (!$this->weight)
            {
                $ret = $this->getHeightOptions(true)[$this->height];
            }
        }

        return $ret;
    }

    /**
     * Возвращает О себе как текст
     */
    public function getAboutText()
    {
        $ret = 'Не указано';
        if (strlen($this->about))
        {
            $ret = $this->about;
        }

        return $ret;
    }

    /**
     * Возвращает вес как текст
     */
    public function getWeightText()
    {
        $ret = '';
        if (isset($this->getWeightOptions(true)[$this->weight]))
        {
            $ret = $this->getWeightOptions(true)[$this->weight] . ' кг';
            if (!$this->weight)
            {
                $ret = $this->getWeightOptions(true)[$this->weight];
            }
        }

        return $ret;
    }

    /**
     * Возвращает ссылку на поиск по населенному пункту, в котором находится анкета
     */
    public function getLinkSearchLocation()
    {
        $profile_list_model = new \xtetis\xdate\models\ProfileListModel([
            'id_location'=>$this->id_geo_object
        ]);
        
        return $profile_list_model->getLinkSearch();
    }

}
