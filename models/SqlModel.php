<?php

namespace xtetis\xdate\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class SqlModel
{

    /**
     * Возвращает DateProfile
     */
    public static function getDateProfileById(
        $id = 0
    )
    {
        $id = intval($id);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('SELECT * FROM xdate_profile WHERE id = :id');

        $stmt->bindParam('id', $id, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->execute();
        $row = $stmt->fetch();

        return $row;
    }

    /**
     * Добавляет DateProfile для указанного пользователя
     */
    public static function addDateProfile(
        $id_user = 0
    )
    {
        $id_user = intval($id_user);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('INSERT INTO xdate_profile (id_user) VALUES(:id_user); ');

        $stmt->bindParam('id_user', $id_user, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->execute();
        $row = $stmt->fetch();

        return $row;
    }

    /**
     * Возвращает DateProfile
     */
    public static function getDateProfileByUserId(
        $id_user = 0
    )
    {
        $id_user = intval($id_user);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('SELECT id FROM xdate_profile WHERE id_user = :id_user');

        $stmt->bindParam('id_user', $id_user, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->execute();
        $row = $stmt->fetch();

        return $row;
    }


    /**
     * Возвращает список типов профиля знакомства
     */
    public static function getDateProfileTypeList()
    {
        $cache_id = 'xdate_profile_type_list';
        $cache = \xtetis\xengine\libraries\Cache::getCacheObj();
        $cache_data = $cache->get($cache_id);
        if (!$cache_data)
        {
            $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
            $stmt    = $connect->prepare('SELECT * FROM xdate_profile_type');

            $stmt->execute();
            $row = $stmt->fetchAll();

            $cache->set($cache_id,$row);
        }
        else
        {
            $row = $cache_data;
        }


        return $row;
    }


    /**
     * Выполняем динамический SQL
     */
    public static function dynamicSql(
        $sql = '',
        $fetch_one = true
    )
    {

        $cache_id = 'date_ssql_count_'.md5($sql.'_'.intval($fetch_one));
        $cache = \xtetis\xengine\libraries\Cache::getCacheObj();
        $cache_data = $cache->get($cache_id);
        if (!$cache_data)
        {

            $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
            $stmt    = $connect->prepare($sql);
            $stmt->execute();
            if (!$fetch_one)
            {
                $array = $stmt->fetchAll();
            }
            else
            {
                $array = $stmt->fetch();
            }

            $cache->set($cache_id,$array);
        }
        else
        {
            $array = $cache_data;
        }


        return $array;
    }

    /**
     * Возвращает DateAlbumList по ID пользователя
     */
    public static function getDateAlbumById(
        $id = 0
    )
    {
        $id = intval($id);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('SELECT * FROM xdate_albums WHERE id = :id');

        $stmt->bindParam('id', $id, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->execute();
        $row = $stmt->fetch();

        return $row;
    }


    /**
     * добавляет DateAlbum
     */
    public static function updateDateAlbum(
        $id = 0,
        $fields = []
    )
    {
        $ret = false;

        $id = intval($id);

        $field_names = [
            'name'=>'str',
            'is_main'=>'int',
        ];

        $update_fields = [];
        foreach($field_names as $field_name => $field_type)
        {
            if (isset($fields[$field_name]))
            {
                if ($field_type == 'int')
                {
                    $fields[$field_name] = intval($fields[$field_name]);
                }
                elseif ($field_type == 'str')
                {
                    $fields[$field_name] = strval($fields[$field_name]);
                }

                $update_fields[$field_name] = $field_name.' = :'.$field_name;
            }
        }

        if ($update_fields)
        {
            $sql = implode(', ',$update_fields);
            $sql = 'UPDATE xdate_albums SET '.$sql.' WHERE id='.$id;

            $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
            $stmt    = $connect->prepare($sql);

            foreach ($update_fields as $field_name => $field_pre_value) 
            {
                if ($field_names[$field_name] == 'int')
                {
                    $stmt->bindParam($field_name, $fields[$field_name], \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
                }
                elseif ($field_names[$field_name] == 'str')
                {
                    $stmt->bindParam($field_name, $fields[$field_name], \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 300);
                }
            }

            $stmt->execute();
            $ret = $stmt->fetch();

            // Удаляем кеш согласно тегу
            $cache = \xtetis\xengine\libraries\Cache::getCacheObj();
            $cache->deleteByTag('xdate_albums');
        }

        return $ret;
    }


    
}
