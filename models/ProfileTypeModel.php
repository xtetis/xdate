<?php

namespace xtetis\xdate\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class ProfileTypeModel extends \xtetis\xengine\models\TableModel
{
    /**
     * Имя обслуживаемой таблицы
     */
    public $table_name = 'xdate_profile_type';
    /**
     * ID статьи
     */
    public $id = 0;

    /**
     * Имя
     */
    public $name = '';

    public $text_nooption_sekected = 'Не указано';

    /**
     * Результат получения данных из SQL запроса
     *
     * getById - Возвращает статью по ID
     * saveArticle - Сохраняем статью
     */
    public $result_sql = [];


    /**
     * Возвращает информацию о статье по ID
     */
    public function getById(
    )
    {
        if ($this->getErrors())
        {
            return false;
        }

        if (!$this->getModelById())
        {
            return false;
        }

        return true;
    }

    /**
     * Возвращает список категорий статей в виде опций для select
     */
    public function getOptions(
        $add_root = true
    )
    {

        $ret = [];

        if ($this->getErrors())
        {
            return false;
        }

        // Возвращает список типов профиля знакомства
        $this->result_sql['getDateProfileTypeList'] = \xtetis\xdate\models\SqlModel::getDateProfileTypeList(
        );

        if (!$this->result_sql['getDateProfileTypeList'])
        {

            $this->addError('getDateProfileTypeList', __FUNCTION__ . ': Не найдены записи в xdate_profile_type');

            return false;
        }

        if ($add_root)
        {
            $ret[0] = $this->text_nooption_sekected;
        }

        foreach ($this->result_sql['getDateProfileTypeList'] as $item) {
            $ret[$item['id']] = $item['name'];
        }
        

        return $ret;
    }
}
