<?php

namespace xtetis\xdate\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class DateAlbumModel extends \xtetis\xengine\models\TableModel
{

    /**
     * ID
     */
    public $id = 0;

    /**
     * Таблица по которой работает модель
     */
    public $table_name = 'xdate_albums';

    /**
     * ID пользователя
     */
    public $id_user = 0;

    /**
     * ID Img Album
     */
    public $id_gallery = 0;

    /**
     * Является ли галерея главной
     */
    public $is_main = 0;

    /**
     * Скрытая ли галерея
     */
    public $hidden = 0;

    /**
     * Имя альбома
     */
    public $name = '';

    /**
     * Результат получения данных из SQL запроса
     */
    public $result_sql = [];

    /**
     * Модель галереи
     */
    public $model_gallery = false;

    /**
     * Список моделей альбомов
     */
    public $date_album_model_by_user_list = [];

    public function __construct($params = [])
    {


        parent::__construct($params);

        // Проверяет параметры
        \xtetis\xdate\Config::validateParams();
    }


    /**
     * Валидируется имя альбома
     */
    public function validateName()
    {

        if ($this->getErrors())
        {
            return false;
        }

        if (strip_tags($this->name) != $this->name)
        {
            $this->addError('name', 'Недопустимы HTML теги');

            return false;
        }

        mb_regex_encoding('UTF-8');
        $tmp_name = preg_replace('/([^\w\ \-\+]+)/iu', '', $this->name);
        if ($tmp_name != $this->name)
        {
            $this->addError('name', 'В имени не допускаются спецсимволы');

            return false;
        }

        $this->name = trim($this->name);

        if (mb_strlen($this->name) < 2)
        {
            $this->addError('name', 'Минимальная длина имени 2 символа');

            return false;
        }

        if (mb_strlen($this->name) > 100)
        {
            $this->addError('name', 'Максимальная длина имени 100 символов');

            return false;
        }

        return true;
    }

    /**
     * Возвращает информацию по ID
     */
    public function getById()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);



        if (!$this->id)
        {
            $this->addError('id', __FUNCTION__ . ': не указан id');

            return false;
        }

        // Возвращает статью по ID
        $this->result_sql['getDateAlbumById'] = \xtetis\xdate\models\SqlModel::getDateAlbumById(
            $this->id
        );

        if (!$this->result_sql['getDateAlbumById'])
        {

            $this->addError('getDateAlbumById', __FUNCTION__ . ': Записо в xdate_albums не существует');

            return false;
        }

        $this->id_user    = $this->result_sql['getDateAlbumById']['id_user'];
        $this->id_gallery = $this->result_sql['getDateAlbumById']['id_gallery'];
        $this->is_main    = $this->result_sql['getDateAlbumById']['is_main'];
        $this->hidden     = $this->result_sql['getDateAlbumById']['hidden'];
        $this->name       = $this->result_sql['getDateAlbumById']['name'];

        return true;
    }

    /**
     * Добавляет альбом в список альбомов пользователя
     */
    public function addDateAlbum()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_user = intval($this->id_user);
        $this->name    = strval($this->name);
        $this->name    = trim($this->name);

        if (!$this->id_user)
        {
            $this->addError('id_user', __FUNCTION__ . ': не указан id_user');

            return false;
        }

        // Валидируем имя
        if (!$this->validateName())
        {
            return false;
        }

        // Получаем список уже имеющихся альбомов, чтобы понять - это главный альбом или нет
        $model_date_album_list = \xtetis\xdate\models\DateAlbumModel::getDateAlbumModelListByUserId($this->id_user);

        // Создаваемый альбом - заглавный
        $this->is_main = 0;
        if (!count($model_date_album_list))
        {
            $this->is_main = 1;
        }

        $model_gallery = new \xtetis\ximg\models\GalleryModel([
            'id_category' => ID_DATE_ALBUM_PARENT_CATEGORY,
            'name'      => 'user_album_' . $this->id_user,
        ]);
        
        if (!$model_gallery->addGallery())
        {
            $this->addError('model_gallery', __FUNCTION__ . ': ошибка при создании галереи для альбома пользователя ' . $model_gallery->getLastErrorMessage());

            return false;
        }

        $this->id_gallery = $model_gallery->id;

        if ($this->is_main)
        {
            $this->name = 'Главный альбом';
        }

        $this->insert_update_field_list = [
            'name',
            'id_user',
            'id_gallery',
            'is_main',
        ];

        if (!$this->insertTableRecord())
        {
            return false;
        }


        return true;
    }

    /**
     * Возвращает модель галереи
     */
    public function getModelGallery()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_gallery    = intval($this->id_gallery);
        $this->model_gallery = \xtetis\ximg\models\GalleryModel::generateModelById($this->id_gallery);

        return $this->model_gallery;
    }

    /**
     * Проверяет - принадлежит ли альбом текущему пользователю
     */
    public function checkIsOwnerCurrentUser()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $model_user = \xtetis\xuser\Component::isLoggedIn();
        if (intval($model_user->id) != $this->id_user)
        {
            $this->addError('id_user', __FUNCTION__ . ': Альбом не принадлежит текущему пользователю');

            return false;
        }

        return true;
    }

    /**
     * Проверяет, что текущий альюом не является главным
     */
    public function checkIsNotMain()
    {
        if ($this->getErrors())
        {
            return false;
        }

        if ($this->is_main)
        {
            $this->addError('id_user', __FUNCTION__ . ': Текущий альбом является главным');

            return false;
        }

        return true;
    }


    /**
     * Редактируем имя альбома
     */
    public function editDateAlbumName()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->name = strval($this->name);
        $this->name = trim($this->name);

        // Валидируем имя
        if (!$this->validateName())
        {
            return false;
        }

        $this->id = intval($this->id);

        if (!$this->id)
        {
            $this->addError('id', __FUNCTION__ . ': не указан id');

            return false;
        }

        // Обновляет альбом по ID
        $this->result_sql['updateDateAlbum'] = \xtetis\xdate\models\SqlModel::updateDateAlbum(
            $this->id,
            [
                'name' => $this->name,
            ]
        );

        return true;

    }





    /**
     * Возвращает массов моделей альбомов профиля по ID пользователя
     */
    public static function getDateAlbumModelListByUserId($id_user = 0)
    {
        $id_user = intval($id_user);

        $sql = "SELECT id FROM xdate_albums WHERE id_user = :id_user";
        $params = [
            'id_user'=>$id_user
        ];

        return self::getModelListBySql(
            $sql,
            $params,
            [
                'cache'=>true,
                'cache_tags'=>[
                    'xdate_albums'
                ]
            ]
        );
    }


}
