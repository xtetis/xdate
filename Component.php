<?php

namespace xtetis\xdate;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

class Component extends \xtetis\xengine\models\Component
{

    /**
     * Список роутов для компонента xdate
     */
    public static function getComponentRoutes()
    {
        $routes = [

            // https://lsfinder.com/date/geo_moscow
            [
                'url'      => '/^\/(' . self::getComponentUrl() . ')\/geo_(\w+)$/',
                'function' => function (
                    $params = []
                )
                {
                    //print_r($params); exit;
                    $sef_geo_object = isset($params[1]) ? $params[1] : '';
                    $model_geo_obj  = \xtetis\xgeo\models\ObjectModel::getModelBySef($sef_geo_object);
                    if (!$model_geo_obj)
                    {
                        http_response_code(404);
                        \xtetis\xengine\helpers\LogHelper::customDie('Населенный пункт не найден');
                    }

                    \xtetis\xengine\App::getApp()->component_name = isset($params[0]) ? $params[0] : '';
                    \xtetis\xengine\App::getApp()->action_name    = 'default';
                    \xtetis\xengine\App::getApp()->query_name     = 'index';

                    $_GET['id_location'] = $model_geo_obj->id;
                },
            ],

            // https://lsfinder.com/date/profile/index/15
            [
                'url'      => '/^\/' . self::getComponentUrl() . '\/profile\/index\/(\d+)$/',
                'function' => function (
                    $params = []
                )
                {
                    $id = isset($params[0]) ? $params[0] : '';
                    $id = intval($id);

                    $model = \xtetis\xdate\models\ProfileModel::generateModelById($id);
                    if ($model)
                    {
                        header('HTTP/1.1 301 Moved Permanently');
                        header('Location: ' . $model->getLink());
                        exit();

                    }
                    else
                    {
                        header('HTTP/1.1 301 Moved Permanently');
                        header('Location: /' . self::getComponentUrl());
                        exit();
                    }
                },
            ],

            [
                'url'      => '/^\/(' . self::getComponentUrl() . ')(\/(\w+)?(\/(\d+)?)?)?$/',
                'function' => function (
                    $params = []
                )
                {
                    \xtetis\xengine\App::getApp()->component_name = isset($params[0]) ? $params[0] : '';
                    \xtetis\xengine\App::getApp()->action_name    = isset($params[2]) ? $params[2] : '';
                    \xtetis\xengine\App::getApp()->query_name     = 'index';

                    $id = isset($params[4]) ? $params[4] : '';

                    if (strlen($id))
                    {
                        $_GET['id'] = $id;
                    }
                },
            ],
            [
                'url'      => '/^\/(' . self::getComponentUrl() . ')(\/(\w+)(\/(\w+)(\/(\d+)(\/(\d+)?)??)?)?)?$/',
                'function' => function (
                    $params = []
                )
                {
                    \xtetis\xengine\App::getApp()->component_name = isset($params[0]) ? $params[0] : '';
                    \xtetis\xengine\App::getApp()->action_name    = isset($params[2]) ? $params[2] : '';
                    \xtetis\xengine\App::getApp()->query_name     = isset($params[4]) ? $params[4] : '';

                    $id  = isset($params[6]) ? $params[6] : '';
                    $id2 = isset($params[8]) ? $params[8] : '';

                    if (strlen($id))
                    {
                        $_GET['id'] = $id;
                    }

                    if (strlen($id2))
                    {
                        $_GET['id2'] = $id2;
                    }
                },
            ],
        ];

        return $routes;
    }

    /**
     * Возвращает блок с последними анкетами
     */
    public static function getBlockLatestAnkets($limit = 6)
    {

        // Получаем список моделей профилей знакомств
        //*****************************************************
        $model_data_profile_list = new \xtetis\xdate\models\ProfileListModel(
            [
                'offset' => 0,
                'limit'  => intval($limit),
            ]
        );
        $model_data_profile_list->getProfileModelList();
        if ($model_data_profile_list->getErrors())
        {
            \xtetis\xengine\helpers\LogHelper::customDie($model_data_profile_list->getLastErrorMessage());
        }
        //*****************************************************

        // Рендерим блок с последними анкетами

        return self::renderBlock('blocks/list_ankets',
            [
                'model_data_profile_list' => $model_data_profile_list,
            ]);

    }

    /**
     * Возвращает блок с анкетами по параметрам
     */
    public static function getBlockAnketsRandom(
        $params = [
            'limit'     => 3,
            'where_arr' => [],
        ]
    )
    {

        $limit     = isset($params['limit']) ? intval($params['limit']) : 6;
        $where_arr = isset($params['where_arr']) ? $params['where_arr'] : [];

        // Получаем список моделей профилей знакомств
        //*****************************************************
        $model_data_profile_list = new \xtetis\xdate\models\ProfileListModel(
            [
                'offset'    => 0,
                'limit'     => intval($limit),
                'order'     => 'RAND()',
                'where_arr' => $where_arr,
            ]
        );
        $model_data_profile_list->getProfileModelList();
        if ($model_data_profile_list->getErrors())
        {
            \xtetis\xengine\helpers\LogHelper::customDie($model_data_profile_list->getLastErrorMessage());
        }
        //*****************************************************

        // Рендерим блок с последними анкетами

        return self::renderBlock('blocks/list_ankets',
            [
                'model_data_profile_list' => $model_data_profile_list,
            ]);

    }

    /**
     * Возвращает массив ссылок для карты сайта
     */
    public static function getSitemapUrlList():array
    {
        $ret = [];

        $url = self::makeUrl(['with_host' => true]);
        $ret[$url] = 'Знакомства';

        $url = self::makeUrl([
            'path'      => [
                'party',
            ],
            'with_host' => true,
        ]);

        $ret[$url] = 'Вечеринки';

        $sql        = 'SELECT `id` FROM `xdate_profile` WHERE COALESCE(`id_profile_type`,0) > 0';
        $model_list = \xtetis\xdate\models\ProfileModel::getModelListBySql($sql);
        foreach ($model_list as $model_profile)
        {
            $link       = $model_profile->getLink(['with_host' => true]);
            $anchor     = $model_profile->getModelUser()->getUserLoginOrName();
            $anchor.=' '.$model_profile->getProfileAgeYearsText().' '.$model_profile->getAgeYearsSuffix();
            $anchor.=' из '.$model_profile->getProfileLocationText('родительный');
            $ret[$link] = 'Анкета знакомств: '.$anchor;
        }

        $sql        = 'SELECT id FROM `xgeo_object` WHERE `sef` IS NOT NULL AND `id` IN ( SELECT DISTINCT `id_geo_object` FROM `xdate_profile` );';
        $model_list = \xtetis\xgeo\models\ObjectModel::getModelListBySql($sql);
        foreach ($model_list as $model_geo_object)
        {
            $link       = \xtetis\xdate\Component::makeUrl([
                'path'      => [
                    'geo_' . $model_geo_object->sef,
                ],
                'with_host' => true,
            ]);
            $anchor     = 'Свинг знакомства в '.$model_geo_object->name;
            $ret[$link] = $anchor;
        }

        return $ret;
    }

}
